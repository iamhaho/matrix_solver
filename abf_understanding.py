#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Thu Dec  5 10:44:20 2019

@author: heepark
"""
#import pyamg
import utility as util
import importlib
import numpy as np
import scipy.sparse as sp
import scipy.sparse.linalg as sla
import numpy.linalg as nla
#importlib.reload(util)

'''
## testing integer matrix 3 variables
a1 = np.array([5,4,3])
a2 = np.array([7,6,5,4,3,2])
a3 = np.array([1,2,3,4,5,6,7,8,9])
a4 = np.array([2,3,4,5,6,7])
a5 = np.array([3,4,5])
#a1 = np.array([5])
#a2 = np.array([0,6])
#a3 = np.array([1,1,0])
#a4 = np.array([2,4])
#a5 = np.array([3])

#m = a1.shape[0]
#A = sp.diags([a1,a2,a3,a4,a5],[-2*m,-m,0,m,2*m])

D1 = sp.diags(a3[0:m])
D2 = sp.diags(a4[0:m])
D3 = sp.diags(a5[0:m])
D4 = sp.diags(a2[0:m])
D5 = sp.diags(a3[m:2*m])
D6 = sp.diags(a4[m:2*m])
D7 = sp.diags(a1[0:m])
D8 = sp.diags(a2[m:2*m])
D9 = sp.diags(a3[2*m:3*m])

Ainv = sla.inv(A)
L = D1*(D5*D9-D6*D8) - D2*(D4*D9-D6*D7) + D3*(D4*D8-D5*D7)
Linv = sla.inv(L)


DD1 = D5*D9-D8*D6
DD2 = -(D2*D9-D8*D3)
DD3 = D2*D6-D5*D3
DD4 = -(D4*D9-D7*D6)
DD5 = D1*D9-D7*D3
DD6 = -(D1*D6-D4*D3)
DD7 = D4*D8-D7*D5
DD8 = -(D1*D8-D7*D2)
DD9 = D1*D5-D4*D2

d1 = DD7.diagonal(0)
d2 = np.append(DD4.diagonal(0),DD8.diagonal(0))
d3 = np.append(DD1.diagonal(0),DD5.diagonal(0))
d3 = np.append(d3,DD9.diagonal(0))
d4 = np.append(DD2.diagonal(0),DD6.diagonal(0))
d5 = DD3.diagonal(0)

D = sp.diags([d1,d2,d3,d4,d5],[-2*m,-m,0,m,2*m])

linv = np.append(Linv.data, Linv.data)
Linv = np.append(linv,Linv.data)
Linv = sp.diags(Linv)

Dinv = Linv*D


'''

'''
## testing integer matrix 2 variables
a1 = np.array([5,4,3])
a2 = np.array([7,6,5,4,3,2])
a5 = np.array([3,4,5])
A = sp.diags([a1,a2,a5],[-3,0,3])

D1 = sp.diags(a2[0:3])
D2 = sp.diags(a5[0:3])
D3 = sp.diags(a1[0:3])
D4 = sp.diags(a2[3:6])

Ainv = sla.inv(A)
L = D1*D4 - D2*D3
Linv = sla.inv(L)

d1 = -D3.diagonal(0)
d2 = np.append(D4.diagonal(0),D1.diagonal(0))
d3 = -D2.diagonal(0)

D = sp.diags([d1,d2,d3],[-3,0,3])

Linv = np.append(Linv.data, Linv.data)
Linv = sp.diags(Linv)

Dinv = Linv*D

print(sla.norm(Ainv - Dinv) < 1.e-14)
'''

## testing random matrix 2 variables
nx=7
ny=5
nvar=2
A = util.create_test_2d_matrix_rand(nx,ny,nvar)
newA = util.rearrange_matrix_2d_wipp_flow(A,nx,ny)

n = nx*ny

#plot.plot_matrix(A)
#plot.plot_matrix(newA)

newA1 = newA[0:n,0:n]
newA2 = newA[0:n,n:n*2]
newA3 = newA[n:n*2,0:n]
newA4 = newA[n:n*2,n:n*2]

D1 = newA1.diagonal(0)
D2 = newA2.diagonal(0)
D3 = newA3.diagonal(0)
D4 = newA4.diagonal(0)

D14 = np.append(D1,D4)
D = sp.diags([D3,D14,D2],[-n,0,n])
#plot.plot_matrix(D)

Dinv = sla.inv(D)
plot.plot_matrix(Dinv)

L = D1*D4 - D2*D3
Linv = sla.inv(sp.diags(L))

d1 = -D3
d2 = np.append(D4,D1)
d3 = -D2

Dabf = sp.diags([d1,d2,d3],[-n,0,n])
Linv = np.append(Linv.data, Linv.data)
Linv = sp.diags(Linv)

Dinv_abf = Linv*Dabf
plot.plot_matrix(Dinv_abf)

norm = sla.norm(Dinv_abf - Dinv)
if norm < 5.e-14:
    print("True")
else:
    print("False: %1.5e" % norm)
        

