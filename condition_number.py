#!/usr/bin/env python2
# -*- coding: utf-8 -*-
"""
Created on Thu May  2 17:12:33 2019

@author: heepark
"""

import numpy as np
import scipy.sparse as sp
import scipy.sparse.linalg as sla
import numpy.linalg as nla
import pyamg
import solve
import load_data as load
import utility as util
import plot
import inspect
import time
import preconditioner as pre


data_path = '/home/heepark/models/wipp/2d/s5_r2_v001/bcgs-ilu0-debug-1core/step290/'
matrix_name = 'WFjacobian_ts291_tc0_ni2.bin'
vector_name = 'WFresidual_ts291_tc0_ni2.bin'
nx = 68
ny = 33

A = load.petsc_binary_as_csr(data_path, matrix_name)
b = load.petsc_binary_vector(data_path, vector_name)

newA = util.rearrange_matrix_2d_wipp_flow(A,nx,ny)
newb = util.rearrange_vector_wipp_flow(b)

#cond_A = pyamg.util.linalg.cond(A)  # 3.3913623367615803e+19
#cond_newA = pyamg.util.linalg.cond(newA)  # 3.174847800645165e+19
'''
M_ilu = pre.ilu_preconditioner(newA)
M_ilu_inv = M_ilu*np.eye(M_ilu.shape[0])
newA_ilu =newA* M_ilu_inv
cond_newA_ilu = pyamg.util.linalg.cond(newA_ilu)  # 11331014.451200135
'''

'''
M_cpr1 = pre.cpr1_ABF_preconditioner(newA)
M_cpr1_inv = np.zeros(M_cpr1.shape)
eye_mat = np.eye(M_cpr1.shape[0], dtype=M_cpr1.dtype)
for i in range(M_cpr1.shape[0]):
    M_cpr1_inv[:,i] = M_cpr1*eye_mat[:,i]

A_cpr1 = newA*M_cpr1_inv
cond_A_cpr1 = pyamg.util.linalg.cond(A_cpr1)  # 2740715498524.427
'''

'''
M_cpr2 = pre.cpr2_ABF_preconditioner(newA)
M_cpr2_inv = np.zeros(M_cpr2.shape)
eye_mat = np.eye(M_cpr2.shape[0], dtype=M_cpr2.dtype)
for i in range(M_cpr2.shape[0]):
    M_cpr2_inv[:,i] = M_cpr2*eye_mat[:,i]
    if i % 100 == 0: print i

A_cpr2 = newA*M_cpr2_inv
cond_A_cpr2 = pyamg.util.linalg.cond(A_cpr2)  # 1934801052.299862
'''

'''
M_ilu = pre.ilu_preconditioner(A)
M_ilu_inv = M_ilu*np.eye(M_ilu.shape[0])
A_ilu = A*M_ilu_inv
cond_A_ilu = pyamg.util.linalg.cond(A_ilu)  # 4587491430.2678795
'''

# ILU(0)
M_ilu = pre.my_ilu_preconditioner(A)
M_ilu_inv = M_ilu*np.eye(M_ilu.shape[0])
A_ilu = A*M_ilu_inv
cond_A_ilu = pyamg.util.linalg.cond(A_ilu)  # 4587491430.2678795

'''
# two iteration scheme
M_cpr1 = pre.cpr1_ABF_preconditioner(newA,2)
M_cpr1_inv = np.zeros(M_cpr1.shape)
eye_mat = np.eye(M_cpr1.shape[0], dtype=M_cpr1.dtype)
for i in range(M_cpr1.shape[0]):
    M_cpr1_inv[:,i] = M_cpr1*eye_mat[:,i]
    if i % 1000 == 0: print i

A_cpr1 = newA*M_cpr1_inv
cond_A_cpr1 = pyamg.util.linalg.cond(A_cpr1)  # 295659850.9919576
'''

'''
M_cpr2 = pre.cpr2_ABF_preconditioner(newA,2)
M_cpr2_inv = np.zeros(M_cpr2.shape)
eye_mat = np.eye(M_cpr2.shape[0], dtype=M_cpr2.dtype)
for i in range(M_cpr2.shape[0]):
    M_cpr2_inv[:,i] = M_cpr2*eye_mat[:,i]
    if i % 1000 == 0: print i

A_cpr2 = newA*M_cpr2_inv
cond_A_cpr2 = pyamg.util.linalg.cond(A_cpr2)  # 4108655.6419483405
'''

