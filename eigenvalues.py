#!/usr/bin/env python2
# -*- coding: utf-8 -*-
"""
Created on Fri May  3 11:07:19 2019

@author: heepark
"""


import numpy as np
import scipy.sparse as sp
import scipy.sparse.linalg as sla
import numpy.linalg as nla
import pyamg
import solve
import load_data as load
import utility as util
import plot
import inspect
import time
import preconditioner as pre




data_path = '/home/heepark/models/wipp/2d/s5_r2_v001/bcgs-ilu0-debug-1core/step290/'
matrix_name = 'WFjacobian_ts291_tc0_ni2.bin'
vector_name = 'WFresidual_ts291_tc0_ni2.bin'
saveimgpath = '/home/heepark/models/wipp/2d/s5_r2_v001/bcgs-ilu0-debug-1core/step290/images/'
nx = 68
ny = 33

A = load.petsc_binary_as_csr(data_path, matrix_name)
b = load.petsc_binary_vector(data_path, vector_name)

newA = util.rearrange_matrix_2d_wipp_flow(A,nx,ny)
newb = util.rearrange_vector_wipp_flow(b)

#w, v = nla.eig(A.toarray())
#neww, newv = nla.eig(newA.toarray())
plot.plot_eigv(w, scale=1e-10, xlim=[-200,200], ylim=[-200,200],
               savefig=saveimgpath+'original_A', title='Original A')
plot.plot_eigv(neww, scale=1e-10, xlim=[-2000,2000], ylim=[-2000,2000],
               savefig=saveimgpath+'new_A', title='Restructured A')

'''
M_ilu = pre.ilu_preconditioner(A)
M_ilu_inv = M_ilu*np.eye(M_ilu.shape[0])
A_ilu = A*M_ilu_inv
ilu_w, ilu_v = nla.eig(A_ilu)  # 4587491430.2678795
'''
plot.plot_eigv(ilu_w, scale=1e-10, xlim=[-2,2], ylim=[-2,2],
               savefig=saveimgpath+'ilu_original_A', title='SuperLU ILU A')

#M_ilu = pre.ilu_preconditioner(newA)
#M_ilu_inv = M_ilu*np.eye(M_ilu.shape[0])
#newA_ilu =newA* M_ilu_inv
#newilu_w, newilu_v = nla.eig(newA_ilu)
plot.plot_eigv(newilu_w, scale=1e-10, xlim=[-2,2], ylim=[-2,2],
               savefig=saveimgpath+'ilu_new_A', 
               title='SuperLU ILU restructured A')

'''
M_cpr1 = pre.cpr1_ABF_preconditioner(newA)
M_cpr1_inv = np.zeros(M_cpr1.shape)
eye_mat = np.eye(M_cpr1.shape[0], dtype=M_cpr1.dtype)
for i in range(M_cpr1.shape[0]):
    M_cpr1_inv[:,i] = M_cpr1*eye_mat[:,i]
    if i % 1000 == 0: print i
A_cpr1 = newA*M_cpr1_inv
cpr1_w, cpr1_v = nla.eig(A_cpr1)
'''
plot.plot_eigv(cpr1_w, scale=1e-10, xlim=[-2,2], ylim=[-2,2],
               savefig=saveimgpath+'cpr1_A', title='CPR-AMG(1) A')

'''
M_cpr2 = pre.cpr2_ABF_preconditioner(newA)
M_cpr2_inv = np.zeros(M_cpr2.shape)
eye_mat = np.eye(M_cpr2.shape[0], dtype=M_cpr2.dtype)
for i in range(M_cpr2.shape[0]):
    M_cpr2_inv[:,i] = M_cpr2*eye_mat[:,i]
    if i % 1000 == 0: print i
A_cpr2 = newA*M_cpr2_inv
cpr2_w, cpr2_v = nla.eig(A_cpr2) 
'''
plot.plot_eigv(cpr2_w, scale=1e-10, xlim=[-2,2], ylim=[-2,2],
               savefig=saveimgpath+'cpr2_A', title='CPR-AMG(2) A')


'''
# two iteration scheme
M_cpr12 = pre.cpr1_ABF_preconditioner(newA,2)
M_cpr12_inv = np.zeros(M_cpr12.shape)
eye_mat = np.eye(M_cpr12.shape[0], dtype=M_cpr12.dtype)
for i in range(M_cpr12.shape[0]):
    M_cpr12_inv[:,i] = M_cpr12*eye_mat[:,i]
    if i % 1000 == 0: print i
A_cpr12 = newA*M_cpr12_inv
cpr12_w, cpr12_v = nla.eig(A_cpr12)
'''
plot.plot_eigv(cpr12_w, scale=1e-10, xlim=[-2,2], ylim=[-2,2],
               savefig=saveimgpath+'cpr12_A', title='CPR1_2iter A')

'''
# two iteration scheme
M_cpr22 = pre.cpr2_ABF_preconditioner(newA,2)
M_cpr22_inv = np.zeros(M_cpr22.shape)
eye_mat = np.eye(M_cpr22.shape[0], dtype=M_cpr22.dtype)
for i in range(M_cpr22.shape[0]):
    M_cpr22_inv[:,i] = M_cpr22*eye_mat[:,i]
    if i % 1000 == 0: print i
A_cpr22 = newA*M_cpr22_inv
cpr22_w, cpr22_v = nla.eig(A_cpr22)
'''
plot.plot_eigv(cpr22_w, scale=1e-10, xlim=[-2,2], ylim=[-2,2],
               savefig=saveimgpath+'cpr22_A', title='CPR1_2iter A')

