import numpy as np
import scipy.sparse as sp
import matplotlib.pyplot as plt
import matplotlib.colors as colors
import petsc4py as pp
from mpl_toolkits.axes_grid1 import make_axes_locatable
from petsc4py import PETSc as ps
import os

def matlab_ascii_as_csr(data_path, matrix_name):
    data = np.genfromtxt(data_path+matrix_name, skip_header=8, skip_footer=2)
    data[:,0] = data[:,0]-1.0
    data[:,1] = data[:,1]-1.0
    data = sp.coo_matrix((data[:,2],(data[:,0],data[:,1])))
    data = data.tocsr()
    return data

def matlab_ascii_vector(data_path, vector_name):
    data = np.genfromtxt(data_path+vector_name, skip_header=3, skip_footer=1)
    return data


def petsc_binary_as_csr(data_path, matrix_name):
    viewer = ps.Viewer().createBinary(data_path+matrix_name, 'r')
    viewer.pushFormat(1)
    A = ps.Mat().load(viewer)
    ai, aj, av = A.getValuesCSR()
    A = sp.csr_matrix((av, aj, ai))
    return A

def petsc_binary_vector(data_path, vector_name):
    viewer = ps.Viewer().createBinary(data_path+vector_name, 'r')
    xx = ps.Vec().load(viewer)
    return xx.getArray()

def convert_petsc_bin_to_npz(data_path):
    for root, dirs, files in os.walk(data_path):
        for f in files:
            if f.endswith('bin'):
                if f.startswith('Gj'):
                    csr_mat = petsc_binary_as_csr(data_path, f)
                    sp.save_npz(f,csr_mat)
                else:
                    vec = petsc_binary_vector(data_path, f)
                    np.save(f,vec)
