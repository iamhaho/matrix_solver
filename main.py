import numpy as np
import scipy.sparse as sp
import scipy.sparse.linalg as sla
import numpy.linalg as nla
import solve
import load_data as load
import utility as util
import plot

import petsc4py
import sys
petsc4py.init(sys.argv)
from petsc4py import PETSc
from timeit import default_timer as time
import pyamg


#data_path = '/home/heepark/models/wipp/3d/s1_r3_v078/bcgs-ilu0-debug-1core/'
'''
data_path = '/home/heepark/models/wipp/2d/s5_r2_v001/bcgs-ilu0-debug-1core/step290/'
matrix_name = 'WFjacobian_ts291_tc0_ni0.bin'
vector_name = 'WFresidual_ts291_tc0_ni0.bin'
solution_name0 = 'WFxx_ts291_tc0_ni0.bin'
solution_name1 = 'WFxx_ts291_tc0_ni1.bin'
'''

data_path = '/home/heepark/models/wipp/2d/s5_r2_v001/bcgs-ilu0-debug-1core/step343/'
matrix_name = 'WFjacobian_ts344_tc0_ni2.bin'
vector_name = 'WFresidual_ts344_tc0_ni2.bin'
solution_name0 = 'WFxx_ts344_tc0_ni2.bin'
solution_name1 = 'WFxx_ts344_tc0_ni2.bin'

nx = 68
ny = 33



'''
data_path = '/home/heepark/models/simple_wipp/simple_2d/'
matrix_name = 'WFjacobian_ts9_tc0_ni0.bin'
vector_name = 'WFresidual_ts9_tc0_ni0.bin'
nx = 60
ny = 55
'''
A = load.petsc_binary_as_csr(data_path, matrix_name)
b = load.petsc_binary_vector(data_path, vector_name)
#x0 = load.petsc_binary_vector(data_path, solution_name0)
#x1 = load.petsc_binary_vector(data_path, solution_name1)

newA = util.rearrange_matrix_2d_wipp_flow(A,nx,ny)
newb = util.rearrange_vector_wipp_flow(b)
#newx0 = util.rearrange_vector_wipp_flow(x0)
#newx1 = util.rearrange_vector_wipp_flow(x1)
#pflo_x = x1-x0
plot.plot_matrix(A)
'''
#remove negatives in the diagonal
#And,bnd = util.remove_negative_diagonals(A,b)

direct_x = solve.scipy_direct(newA, newb)


print "\n\ncpr1_true_IMPES"
cpr1_true_IMPES_x = solve.cpr1_true_IMPES(newA, newb, maxiter=5)
print "\n\ncpr1_quasi_IMPES"
cpr1_quasi_IMPES_x = solve.cpr1_quasi_IMPES(newA, newb, maxiter=5)
print "\n\ncpr1_ABF"
cpr1_ABF_x = solve.cpr1_ABF(newA, newb, maxiter=50)

#print "\n\ncpr1_quasi_IMPES"
#cpr2_quasi_IMPES_x = solve.cpr2_quasi_IMPES(newA, newb, maxiter=5)
print "\n\ncpr2_ABF"
cpr2_ABF_x = solve.cpr2_ABF(newA, newb, maxiter=50)

#print "\n cpr2"
#cpr2_x = solve.cpr2(newA, newb)

print "\n\nblock_factorization"
bf_x = solve.block_factorization(newA, newb, maxiter=5)

#amg_x = solve.pyamg_smooth_aggregation(newA,newb)

#bcgs_x, status, info = solve.scipy_bicgstab(newA, newb, tol=1e-10, maxiter=10000,
#                                            preconditioner='ilu')
'''
'''
nx=3
ny=4
nz=3

nvar=3
m = util.create_test_3d_matrix(nx,ny,nz,nvar)
nelem = m.shape[0]/2
plot.plot_matrix(m)
newm = util.rearrange_matrix_3d_wipp_flow(m,nx,ny,nz)
plot.plot_matrix(newm)
'''

'''
tolerance = 1e-9
tmp1 = time()
direct_x1 = solve.scipy_direct(A, b)
direct_x2 = solve.scipy_direct(A, b)
direct_newx = solve.scipy_direct(newA, newb)
tmp2 = time()
#bcgs_x, status, info = solve.scipy_bicgstab(A, b, tol=tolerance, maxiter=10000,
#                                            preconditioner='ilu')
bcgs_x, status, info = solve.scipy_bicgstab(newA, newb, tol=tolerance, maxiter=10000,
                                            preconditioner='ilu')
tmp3 = time()
restol = nla.norm(b)*tolerance
print 'direct solve time: %g s' % (tmp2-tmp1)
print 'iterative solve time: %g s' % (tmp3-tmp2)
print 'tolerance for residual: %e' % (restol)
'''

'''
status = 1
tolerance = 1e-8
util.check_zero_diag(A)
tmp1 = time()
x = solve.scipy_direct(A, b)
tmp2 = time()
#xx, info = solve.pyamg_smooth_aggregation(newA, newb, tol=tolerance)
xx, info = solve.pyamg_block_smooth_aggregation(A, b, tol=tolerance)
xx, status, info = solve.scipy_bicgstab(A, b, tol=tolerance, maxiter=100,
                                        preconditioner='ilu')
tmp3 = time()

restol = nla.norm(b)*tolerance
print 'direct solve time: %g s' % (tmp2-tmp1)
print 'iterative solve time: %g s' % (tmp3-tmp2)
print 'tolerance for residual: %e' % (restol)
if status == 0:
    print 'converged.'
else:
    print 'not converged.'
'''

'''
A - scaled
Ans - not scaled
And - scaled remove neg diag
Arsnd - scaled remove neg diag restructured
Ars - scaled restructured
'''
