import matplotlib.pyplot as plt
import matplotlib.colors as colors
from mpl_toolkits.axes_grid1 import make_axes_locatable
import numpy as np
import pyamg
from timeit import default_timer as time

def plot_eigv(eigv, scale=1, xlim=[], ylim=[], savefig='', title=''):
    reals = eigv.real*scale
    imags = eigv.imag*scale
    plt.scatter(reals,imags,s=5)
    plt.title(title, fontsize=16)
    plt.tick_params(axis='x', labelsize=16)
    plt.tick_params(axis='y', labelsize=16)
    if xlim != []:
        plt.xlim(xlim)
    if ylim != []:
        plt.ylim(ylim)
    if savefig == '':
        plt.show()
    else:
        plt.savefig(savefig)
    plt.close()

def plot_matrix(A,xzoom=None,yzoom=None,savefig=''):
    '''
    Plots coo matrix in colorful manner :) 
    Positive values are in red dots
    Negative values are in blue dots
    It is in log scale.
    A = Matrix in sparse.coo format
    xzoom = [xmin, xmax]
    yzoom = [ymin, ymax]
    savefig = filename in string
    '''
    A = A.tocoo()
    Aposcol = A.col[A.sign().data == 1]
    pospos = A.col[A.sign().data == 1].copy()  # there is some bug with Aposcol
    Anegcol = A.col[A.sign().data == -1].copy()
    Aposrow = A.row[A.sign().data == 1].copy()
    Anegrow = A.row[A.sign().data == -1].copy()
    Aposdata = A.data[A.sign().data == 1].copy()
    Anegdata = A.data[A.sign().data == -1].copy()
    Anegdata = -1. * Anegdata

    if Anegdata.shape[0] != 0:
        vabsmin = min(Aposdata.min(), Anegdata.min())*.99  
    else:
        vabsmin = Aposdata.min()*.99
    if Anegdata.shape[0] != 0:
        vabsmax = max(Aposdata.max(), Anegdata.max())*1.01
    else:
        vabsmax = Aposdata.max()*1.01

    fig = plt.figure(1, figsize=(10,10))
    ax = fig.add_subplot(111)
    ax.set_facecolor('black')
    cmappos = plt.cm.Reds
    cmapneg = plt.cm.Blues

    norm_pos_color = colors.LogNorm(vmin=vabsmin, vmax=vabsmax)
    norm_neg_color = colors.LogNorm(vmin=vabsmin, vmax=vabsmax)
    marker_pos_colors=cmappos(norm_pos_color(Aposdata))
    marker_neg_colors=cmapneg(norm_neg_color(Anegdata))
    #ax.plot(A.col, A.row, 's', color='white', markersize=3)
    ax.scatter(pospos,Aposrow, marker='s', color=marker_pos_colors, s=15)
    ax.scatter(Anegcol,Anegrow, marker='s', color=marker_neg_colors, s=15)
    ax.set_xlim(0, A.shape[1])
    ax.set_ylim(0, A.shape[0])
    ax.set_aspect('equal')
    for spine in ax.spines.values():
        spine.set_visible(False)
    ax.invert_yaxis()
    ax.set_aspect('equal')
    sm_pos = plt.cm.ScalarMappable(cmap=cmappos, norm=norm_pos_color)
    sm_pos.set_array([])
    sm_neg = plt.cm.ScalarMappable(cmap=cmapneg, norm=norm_neg_color)
    sm_neg.set_array([])
    if xzoom == None:
        ax.set_xlim([-1,A.shape[1]+1])
    else:
        ax.set_xlim([xzoom[0]-0.5,xzoom[1]-0.5])
    if yzoom == None:
        ax.set_ylim([A.shape[0]+1,-1])
    else:
        ax.set_ylim([yzoom[1]-0.5,yzoom[0]-0.5])
    divider = make_axes_locatable(ax)
    cax_pos = divider.append_axes("right", size="5%", pad=0.05)
    cax_neg = divider.append_axes("right", size="5%", pad=0.50)
    plt.colorbar(sm_pos, cax=cax_pos)
    plt.colorbar(sm_neg, cax=cax_neg)
    ax.set_title('Matrix A, red(+), blue(-)', fontsize=20)
    ax.set_xlabel('columns', fontsize=16)
    ax.set_ylabel('rows', fontsize=16)
    ax.tick_params(axis='x', labelsize=16)
    ax.tick_params(axis='y', labelsize=16)
    if savefig == '':
        plt.show()
    else:
        plt.savefig(savefig)
    plt.close()
    return ax

def plot_2d_richards_error(A,b,numx,numy,x=None):
    if x==None:
        x=np.random.rand(A.shape[0])
    xj=x.copy()
    xgs=x.copy()
    xs=x.copy()
    xbj=x.copy()
    xbgs=x.copy()
    xsor=x.copy()
    xne=x.copy()
    xnr=x.copy()
    plt.figure(1,figsize=[16,12])
    
    ax1 = plt.subplot(3,3,1)
    ax1_plot = ax1.pcolormesh(x.reshape(numy,numx))
    ax1.set_xlim([0,numx])
    ax1.set_ylim([0,numy])
    ax1.title.set_text('Random error')
    plt.colorbar(ax1_plot, ax=ax1)
#=======================================================
    ax2 = plt.subplot(3,3,2)
    tmpj1 = time()
    pyamg.relaxation.relaxation.jacobi(A, xj, b, iterations=5, omega=4/3)
    tmpj2 = time()
    took2 = tmpj2-tmpj1
    ax2_plot = ax2.pcolormesh(xj.reshape(numy,numx))
    ax2.set_xlim([0,numx])
    ax2.set_ylim([0,numy])
    ax2.title.set_text('Weight Jacobi on A: %.3e s' % took2)
    plt.colorbar(ax2_plot, ax=ax2)
#=======================================================
    ax3 = plt.subplot(3,3,3)
    tmpgs1 = time()
    pyamg.relaxation.relaxation.gauss_seidel(A, xgs, b, iterations=3)
    tmpgs2 = time()
    took3 = tmpgs2 -tmpgs1
    ax3_plot = ax3.pcolormesh(xgs.reshape(numy,numx))
    ax3.set_xlim([0,numx])
    ax3.set_ylim([0,numy])
    ax3.title.set_text('Gauss_Seidel on A: %.3e s' % took3)
    plt.colorbar(ax3_plot, ax=ax3)
#=======================================================
    ax4 = plt.subplot(3,3,4)
    tmps1 = time()
    pyamg.relaxation.relaxation.schwarz(A, xs, b, iterations=2)
    tmps2 = time()
    took4 = tmps2-tmps1
    ax4_plot = ax4.pcolormesh(xs.reshape(numy,numx))
    ax4.set_xlim([0,numx])
    ax4.set_ylim([0,numy])
    ax4.title.set_text('Schwarz on A: %.3e s' % took4)
    plt.colorbar(ax4_plot, ax=ax4)
#=======================================================
    ax5 = plt.subplot(3,3,5)
    tmpbj1 = time()
    pyamg.relaxation.relaxation.block_jacobi(A, xbj, b, iterations=5,
                                             blocksize=3, omega=4/3)
    tmpbj2 = time()
    took5 = tmpbj2-tmpbj1
    ax5_plot = ax5.pcolormesh(xbj.reshape(numy,numx))
    ax5.set_xlim([0,numx])
    ax5.set_ylim([0,numy])
    ax5.title.set_text('Block Jacobi on A: %.3e s' % took5)
    plt.colorbar(ax5_plot, ax=ax5)
#=======================================================
    ax6 = plt.subplot(3,3,6)
    tmpbgs1 = time()
    pyamg.relaxation.relaxation.block_gauss_seidel(A, xbgs, b, iterations=3,
                                                   blocksize=3)
    tmpbgs2 = time()
    took6 = tmpbgs2-tmpbgs1
    ax6_plot = ax6.pcolormesh(xbgs.reshape(numy,numx))
    ax6.set_xlim([0,numx])
    ax6.set_ylim([0,numy])
    ax6.title.set_text('Block GS on A: %.3e s' % took6)
    plt.colorbar(ax6_plot, ax=ax6)
#=======================================================
    ax7 = plt.subplot(3,3,7)
    tmpso1 = time()
    pyamg.relaxation.relaxation.sor(A, xsor, b, iterations=5, omega=4/3)
    tmpso2 = time()
    took7 = tmpso2-tmpso1
    ax7_plot = ax7.pcolormesh(xsor.reshape(numy,numx))
    ax7.set_xlim([0,numx])
    ax7.set_ylim([0,numy])
    ax7.title.set_text('SOR on A: %.3e s' % took7)
    plt.colorbar(ax7_plot, ax=ax7)
#=======================================================
    ax8 = plt.subplot(3,3,8)
    tmpgse1 = time()
    pyamg.relaxation.relaxation.gauss_seidel_ne(A, xne, b, iterations=3,
                                                omega=1.0)
    tmpgse2 = time()
    took8 = tmpgse2-tmpgse1
    ax8_plot = ax8.pcolormesh(xne.reshape(numy,numx))
    ax8.set_xlim([0,numx])
    ax8.set_ylim([0,numy])
    ax8.title.set_text('GS_ne on A: %.3e s' % took8)
    plt.colorbar(ax8_plot, ax=ax8)
#=======================================================
    ax9 = plt.subplot(3,3,9)
    tmpgsr1 = time()
    pyamg.relaxation.relaxation.gauss_seidel_nr(A, xnr, b, iterations=3,
                                                omega=1.0)
    tmpgsr2 = time()
    took9 = tmpgsr2-tmpgsr1
    ax9_plot = ax9.pcolormesh(xnr.reshape(numy,numx))
    ax9.set_xlim([0,numx])
    ax9.set_ylim([0,numy])
    ax9.title.set_text('GS_nr on A: %.3e s' % took9)
    plt.colorbar(ax9_plot, ax=ax9)
#=======================================================
    plt.show()

    
def plot_1d_richards_error(A,b,x=None):
    if x==None:
        x=np.random.rand(A.shape[0])
    xj=x.copy()
    xgs=x.copy()
    xs=x.copy()
    xbj=x.copy()
    xbgs=x.copy()
    xsor=x.copy()
    xne=x.copy()
    xnr=x.copy()
    plt.figure(1,figsize=[16,12])
    
    ax1 = plt.subplot(3,3,1)
    ax1_plot = ax1.plot(x)
    ax1.title.set_text('Random error')
#=======================================================    
    ax2 = plt.subplot(3,3,2)
    tmp = time()
    pyamg.relaxation.relaxation.jacobi(A, xj, b, iterations=20, omega=4/3)
    took = time()-tmp
    ax2_plot = ax2.plot(xj)
    ax2.title.set_text('Weight Jacobi on A: %.3e s' % took)
#=======================================================
    ax3 = plt.subplot(3,3,3)
    tmp = time()
    pyamg.relaxation.relaxation.gauss_seidel(A, xgs, b, iterations=5)
    took = time()-tmp
    ax3_plot = ax3.plot(xgs)
    ax3.title.set_text('Gauss_Seidel on A: %.3e s' % took)
#=======================================================
    ax4 = plt.subplot(3,3,4)
    tmp = time()
    pyamg.relaxation.relaxation.schwarz(A, xs, b, iterations=3)
    took = time()-tmp
    ax4_plot = ax4.plot(xs)
    ax4.title.set_text('Schwarz on A: %.3e s' % took)
#=======================================================
    ax5 = plt.subplot(3,3,5)
    tmp = time()
    pyamg.relaxation.relaxation.block_jacobi(A, xbj, b, iterations=20, omega=4/3)
    took = time()-tmp
    ax5_plot = ax5.plot(xbj)
    ax5.title.set_text('Block Jacobi on A: %.3e s' % took)
#=======================================================
    ax6 = plt.subplot(3,3,6)
    tmp = time()
    pyamg.relaxation.relaxation.block_gauss_seidel(A, xbgs, b, iterations=5)
    took = time()-tmp
    ax6_plot = ax6.plot(xbgs)
    ax6.title.set_text('Block GS on A: %.3e s' % took)
#=======================================================
    ax7 = plt.subplot(3,3,7)
    tmp = time()
    pyamg.relaxation.relaxation.sor(A, xsor, b, iterations=20, omega=4/3)
    took = time()-tmp
    ax7_plot = ax7.plot(xsor)
    ax7.title.set_text('SOR on A: %.3e s' % took)
#=======================================================
    ax8 = plt.subplot(3,3,8)
    tmp = time()
    pyamg.relaxation.relaxation.gauss_seidel_ne(A, xne, b, iterations=5,
                                                omega=1.0)
    took = time()-tmp
    ax8_plot = ax8.plot(xne)
    ax8.title.set_text('GS_ne on A: %.3e s' % took)
#=======================================================
    ax9 = plt.subplot(3,3,9)
    tmp = time()
    pyamg.relaxation.relaxation.gauss_seidel_nr(A, xnr, b, iterations=5,
                                                omega=1.0)
    took = time()-tmp
    ax9_plot = ax9.plot(xnr)
    ax9.title.set_text('GS_nr on A: %.3e s' % took)
#=======================================================
    plt.show()

def plot_2d_general_error(A,b,numx,numy,x=None):
    if x==None:
        x=np.random.rand(A.shape[0])
    #pyamg.relaxation.relaxation.jacobi(A, x, b, iterations=5, omega=4/3)
    #pyamg.relaxation.relaxation.gauss_seidel_ne(A, x, b, iterations=5,
    #                                            omega=4/3)
    pyamg.relaxation.relaxation.schwarz(A, x, b, iterations=3)
    plt.pcolormesh(x.reshape(numy,numx))
    plt.colorbar()
    plt.show()

def plot_1d_general_error(A,b,x=None):
    if x==None:
        x = np.random.rand(A.shape[0])
    #pyamg.relaxation.relaxation.jacobi(A, x, b, iterations=15, omega=2/3)
    #pyamg.relaxation.relaxation.gauss_seidel_ne(A, x, b, iterations=5,
    #                                            omega=2/3)
    #pyamg.relaxation.relaxation.schwarz(A, x, b, iterations=3)
    #pyamg.relaxation.relaxation.block_jacobi(A,x,b,blocksize=3,iterations=10,
    #                                         omega=2/3)
    plt.figure(1,figsize=[14,8])
    plt.plot(x)
    plt.show()
