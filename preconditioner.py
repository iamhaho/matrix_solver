#!/usr/bin/env python2
# -*- coding: utf-8 -*-
"""
Created on Wed Apr 24 17:22:44 2019

@author: heepark
"""

import numpy as np
import scipy.sparse as sp
import scipy.sparse.linalg as sla
import numpy.linalg as nla
import pyamg
import solve
import load_data as load
import utility as util
import plot
import inspect
import time


class iter_counter(object):
    def __init__(self, module):
        self.niter = 0
        self.res = []
        self.module = module
    def __call__(self, xk=None):
        self.niter += 1
        frame = inspect.currentframe().f_back
        if self.module == 'scipy':
            self.res.append(frame.f_locals['resid'])
            print('iter %3i\tres = %e' % (self.niter, self.res[self.niter-1]))
        elif self.module == 'pyamg':
            self.res.append(frame.f_locals['normr'])
            print('iter %3i\tres = %e' % (self.niter, self.res[self.niter-1]))


def jacobi_preconditioner(A):
    M = sp.diags(1/A.diagonal(), format='csr')
    def _matvec(b):
        return M*b
    return sla.LinearOperator(M.shape, _matvec)


def ilu_preconditioner(A, drop_tol=1e-5, fill_factor=10):
    ILU = sla.spilu(A, drop_tol=drop_tol, fill_factor=fill_factor, permc_spec='NATURAL')
    M = sla.LinearOperator(A.shape, ILU.solve)
    return M


def my_ilu_preconditioner(A):
    A = A.tolil()
    n = A.shape[0]
    for k in range(n-1):
        for i in range(k+1,n):
            if abs(A[i,k]) > 1.0e-50:
                A[i,k] = A[i,k]/A[k,k]
            for j in range(k+1,n):
                if abs(A[i,j]) > 1.0e-50:
                    A[i,j] = A[i,j]-A[i,k]*A[k,j]
    
    def _solve(b):
        n = b.shape[0]
        x = np.zeros(n)
        y = np.zeros(n)
        for i in range(n):
            alpha = 0
            for k in range(i):
                alpha = alpha + A[i,k]*y[k]
            y[i] = b[i] - alpha
        for i in range(n-1,-1,-1):
            alpha = 0
            for k in range(i+1,n):
                alpha = alpha + A[i,k]*x[k]
            x[i] = (y[i]- alpha)/A[i,i]
        return x
    
    M = sla.LinearOperator(A.shape, _solve)
    return M


def block_factorization_preconditioner(A, maxiter=1, output=False):
    nelem = A.shape[0]/2
    App = A[0:nelem,0:nelem]
    Aps = A[0:nelem,nelem:nelem*2]
    Asp = A[nelem:nelem*2,0:nelem]
    Ass = A[nelem:nelem*2,nelem:nelem*2]

    # approximate Schur complement
    Ass_diag_inv = sp.diags(1./Ass.diagonal(0), format="csr")
    Sbar = App - (Aps*Ass_diag_inv)*Asp
    # initial guess
    u = np.zeros(A.shape[0])
    def _solve(b):

        niter = 0
        
        while True:
            # step 1
            r = b - A*u

            if niter >= maxiter:
                break
            rp = r[0:nelem]
            rs = r[nelem:nelem*2]
            
            # step 2
            #ml = pyamg.smoothed_aggregation_solver(Ass)
            #snew = ml.solve(rs, accel='gmres')
            res = []
            ml = pyamg.ruge_stuben_solver(Ass)
            snew = ml.solve(rs, accel='gmres', tol=1e-07, residuals=res)
            if output:
                print "    inner iterations: %i, residual: %g" % (len(res), 
                                                                  res[-1]) 
            #snew = sla.spsolve(Ass, rs)
            # step 3
            rnew = rp - Aps*snew
            # step 4
            #ml = pyamg.smoothed_aggregation_solver(Sbar)
            #pnew = ml.solve(rnew, accel='gmres')
            res = []
            ml2 = pyamg.ruge_stuben_solver(Sbar)
            pnew = ml2.solve(rnew, accel='gmres', tol=1e-07, residuals=res)
            if output:
                print "    inner iterations: %i, residual: %g" % (len(res), 
                                                                  res[-1]) 
            #pnew = sla.spsolve(Sbar, rnew)
            
            # update solution
            u[0:nelem] = u[0:nelem] + pnew
            u[nelem:nelem*2] = u[nelem:nelem*2] + snew
            niter += 1
        if output: 
            print "  precond. iter: %i"  % (niter)
        return u
    
    M = sla.LinearOperator(A.shape, _solve)
    return M


def cpr1_quasi_IMPES_preconditioner(A, maxiter=1, output=False):
    nelem = A.shape[0]/2
    App = A[0:nelem, 0:nelem]
    Aps = A[0:nelem, nelem:nelem*2]
    Asp = A[nelem:nelem*2, 0:nelem]
    Ass = A[nelem:nelem*2, nelem:nelem*2]
    u = np.zeros(A.shape[0])

    # quasi-IMPES matrix decoupling
    Ass_diag_inv = sp.diags(1./Ass.diagonal(0), format="csr")
    Aps_diag = sp.diags(Aps.diagonal(0), format="csr")
    Appstar = App - Aps_diag*Ass_diag_inv*Asp
    
    # For step 2
    ILU = sla.spilu(A, permc_spec='NATURAL')
    
    def _solve(b):

        niter = 0
        
        while True:
            # step 1
            r = b - A*u

            if niter >= maxiter:
                break
            
            # step 2
            u_ilu = ILU.solve(r)
            uhalf = u + u_ilu
            
            # step 3
            rhalf = r - A*u_ilu
    
            # step 4
            rp = rhalf[0:nelem]
            rs = rhalf[nelem:nelem*2]
            rp = rp - Aps_diag*Ass_diag_inv*rs
            #ml = pyamg.smoothed_aggregation_solver(Appstar)
            #del_p = ml.solve(rp, accel='gmres',tol=1e-7)
            res = []
            ml = pyamg.ruge_stuben_solver(Appstar)
            del_p = ml.solve(rp, accel='fgmres', tol=1e-04, residuals=res)#, maxiter=20)
            if output:
                print "    inner iterations: %i, residual: %g" % (len(res), 
                                                                  res[-1]) 
            #del_p = sla.spsolve(Appstar, rp)
            
            # step 5
            u[0:nelem] = uhalf[0:nelem] + del_p
            u[nelem:nelem*2] = uhalf[nelem:nelem*2]
            
            niter += 1
        if output: 
            print "  precond. iter: %i"  % (niter)
        return u
    
    M = sla.LinearOperator(A.shape, _solve)
    return M


def cpr1_true_IMPES_preconditioner(A, maxiter=1, output=False):
    nelem = A.shape[0]/2
    App = A[0:nelem, 0:nelem]
    Aps = A[0:nelem, nelem:nelem*2]
    Asp = A[nelem:nelem*2, 0:nelem]
    Ass = A[nelem:nelem*2, nelem:nelem*2]
    u = np.zeros(A.shape[0])

    # true-IMPES matrix conversion decoupling
    # .sum operations is column sum operation
    Ass_colsum_inv = sp.diags(np.array(1./Ass.sum(axis=0))[0], 
                              format='csr')
    Aps_colsum = sp.diags(np.array(Aps.sum(axis=0))[0], format='csr')
    Appstar = App - Aps_colsum*Ass_colsum_inv*Asp
    
    # For step 2
    ILU = sla.spilu(A, permc_spec='NATURAL')
    
    def _solve(b):

        niter = 0
        while True:
            # step 1
            r = b - A*u

            if niter >= maxiter:
                break
            
            # step 2
            u_ilu = ILU.solve(r)
            uhalf = u + u_ilu
            
            # step 3
            rhalf = r - A*u_ilu
    
            # step 4
            rp = rhalf[0:nelem]
            rs = rhalf[nelem:nelem*2]
            rp = rp - Aps_colsum*Ass_colsum_inv*rs
            #ml = pyamg.smoothed_aggregation_solver(Appstar)
            #del_p = ml.solve(rp, accel='gmres',tol=1e-7)
            res = []
            ml = pyamg.ruge_stuben_solver(Appstar)
            del_p = ml.solve(rp, accel='gmres', tol=1e-7, residuals=res)
            if output:
                print "    inner iterations: %i, residual: %g" % (len(res), 
                                                                  res[-1]) 
            #del_p = sla.spsolve(Appstar, rp)
            
            # step 5
            u[0:nelem] = uhalf[0:nelem] + del_p
            u[nelem:nelem*2] = uhalf[nelem:nelem*2]
            
            niter += 1
        if output: 
            print "  precond. iter: %i"  % (niter)
        return u
    
    M = sla.LinearOperator(A.shape, _solve)
    return M


def cpr1_true_IMPES_preconditioner_row(A, maxiter=1, output=False):
    nelem = A.shape[0]/2
    App = A[0:nelem, 0:nelem]
    Aps = A[0:nelem, nelem:nelem*2]
    Asp = A[nelem:nelem*2, 0:nelem]
    Ass = A[nelem:nelem*2, nelem:nelem*2]
    u = np.zeros(A.shape[0])

    # true-IMPES matrix conversion decoupling
    # .sum operations is column sum operation
    Ass_colsum_inv = sp.diags(np.array(1./Ass.sum(axis=1)).flatten(), 
                              format='csr')
    Aps_colsum = sp.diags(np.array(Aps.sum(axis=1)).flatten(), format='csr')
    Appstar = App - Aps_colsum*Ass_colsum_inv*Asp
    
    # For step 2
    ILU = sla.spilu(A, permc_spec='NATURAL')
    
    def _solve(b):

        niter = 0
        while True:
            # step 1
            r = b - A*u

            if niter >= maxiter:
                break
            
            # step 2
            u_ilu = ILU.solve(r)
            uhalf = u + u_ilu
            
            # step 3
            rhalf = r - A*u_ilu
    
            # step 4
            rp = rhalf[0:nelem]
            rs = rhalf[nelem:nelem*2]
            rp = rp - Aps_colsum*Ass_colsum_inv*rs
            #ml = pyamg.smoothed_aggregation_solver(Appstar)
            #del_p = ml.solve(rp, accel='gmres',tol=1e-7)
            res = []
            ml = pyamg.ruge_stuben_solver(Appstar)
            del_p = ml.solve(rp, accel='gmres', tol=1e-7, residuals=res)
            if output:
                print "    inner iterations: %i, residual: %g" % (len(res), 
                                                                  res[-1]) 
            #del_p = sla.spsolve(Appstar, rp)
            
            # step 5
            u[0:nelem] = uhalf[0:nelem] + del_p
            u[nelem:nelem*2] = uhalf[nelem:nelem*2]
            
            niter += 1
        if output: 
            print "  precond. iter: %i"  % (niter)
        return u
    
    M = sla.LinearOperator(A.shape, _solve)
    return M


def cpr1_ABF_preconditioner(A, maxiter=1, output=False):
    nelem = A.shape[0]/2
    App = A[0:nelem, 0:nelem]
    Aps = A[0:nelem, nelem:nelem*2]
    Asp = A[nelem:nelem*2, 0:nelem]
    Ass = A[nelem:nelem*2, nelem:nelem*2]
    u = np.zeros(A.shape[0])

    # Alternate-Block Factorization(ABF) matrix decoupling
    Lambda = App.diagonal(0)*Ass.diagonal(0) - Aps.diagonal(0)*Asp.diagonal(0)
    Lambda_inv = sp.diags(1./Lambda, format='csr')
    Appstar = Lambda_inv*(sp.diags(Ass.diagonal(0), format='csr')*App - 
                          sp.diags(Aps.diagonal(0), format='csr')*Asp)  
    # For step 2
    ILU = sla.spilu(A, permc_spec='NATURAL')
        
    def _solve(b):

        niter = 0
        
        while True:
            # step 1
            r = b - A*u

            if niter >= maxiter:
                break
            
            # step 2
            u_ilu = ILU.solve(r)
            uhalf = u + u_ilu
            
            # step 3
            rhalf = r - A*u_ilu
    
            # step 4
            rp = rhalf[0:nelem]
            rs = rhalf[nelem:nelem*2]
            rp = Lambda_inv*(Ass.diagonal(0)*rp-Aps.diagonal(0)*rs)
            #ml = pyamg.smoothed_aggregation_solver(Appstar)
            #del_p = ml.solve(rp, tol=1e-07)
            res = []
            ml = pyamg.ruge_stuben_solver(Appstar)
            del_p = ml.solve(rp, accel='gmres', tol=1e-07, residuals=res)
            if output:
                print "    inner iterations: %i, residual: %g" % (len(res), 
                                                                  res[-1]) 
            #del_p = sla.spsolve(Appstar, rp)
            # step 5
            u[0:nelem] = uhalf[0:nelem] + del_p
            u[nelem:nelem*2] = uhalf[nelem:nelem*2]
            
            niter += 1
            
        if output: 
            print "  precond. iter: %i"  % (niter)
        return u
    
    M = sla.LinearOperator(A.shape, _solve)
    return M


def cpr2_ABF_preconditioner(A, maxiter=1, output=False):
    nelem = A.shape[0]/2
    App = A[0:nelem, 0:nelem]
    Aps = A[0:nelem, nelem:nelem*2]
    Asp = A[nelem:nelem*2, 0:nelem]
    Ass = A[nelem:nelem*2, nelem:nelem*2]
    u = np.zeros(A.shape[0])

    # Alternate-Block Factorization(ABF) matrix decoupling
    Lambda = App.diagonal(0)*Ass.diagonal(0) - Aps.diagonal(0)*Asp.diagonal(0)
    Lambda_inv = sp.diags(1./Lambda, format='csr')
    Appstar = Lambda_inv*(sp.diags(Ass.diagonal(0), format='csr')*App - 
                          sp.diags(Aps.diagonal(0), format='csr')*Asp)
    Assstar = Lambda_inv*(sp.diags(App.diagonal(0), format='csr')*Ass - 
                          sp.diags(Asp.diagonal(0), format='csr')*Aps)
    
    # For step 2
    ILU = sla.spilu(A, permc_spec='NATURAL')

    def _solve(b):
        
        niter = 0
        while True:
            # step 1
            r = b - A*u

            if niter >= maxiter:
                break
            
            # step 2
            u_ilu = ILU.solve(r)
            uhalf = u + u_ilu
            
            # step 3
            rhalf = r - A*u_ilu
            
            # step 4
            rp = rhalf[0:nelem]
            rs = rhalf[nelem:nelem*2]
              # decouple pressure residual
            rpstar = Lambda_inv*(Ass.diagonal(0)*rp-Aps.diagonal(0)*rs)
            #ml = pyamg.smoothed_aggregation_solver(Appstar)
            #del_p = ml.solve(rpstar, accel='gmres',tol=1e-7)
            #del_p = sla.spsolve(Appstar, rpstar)
            
            res = []
            ml = pyamg.ruge_stuben_solver(Appstar)
            del_p = ml.solve(rpstar, accel='gmres', tol=1e-07, residuals=res)
            if output:
                print "    inner iterations: %i, residual: %g" % (len(res), 
                                                                  res[-1]) 
            
            # step 5
              # decouple pressure residual
            rsstar = Lambda_inv*(App.diagonal(0)*rs-Asp.diagonal(0)*rp)
            #ml = pyamg.smoothed_aggregation_solver(Assstar)
            #del_s = ml.solve(rsstar, accel='gmres',tol=1e-7)
            #del_s = sla.spsolve(Assstar, rsstar)
            
            res = []
            ml = pyamg.ruge_stuben_solver(Assstar)
            del_s = ml.solve(rsstar, accel='gmres', tol=1e-07, residuals=res)
            if output:
                print "    inner iterations: %i, residual: %g" % (len(res), 
                                                                  res[-1]) 
                
            # step 6
            u[0:nelem] = uhalf[0:nelem] + del_p
            u[nelem:nelem*2] = uhalf[nelem:nelem*2] + del_s
    
            niter += 1
        
        if output: 
            print "  precond. iter: %i"  % (niter)
        return u
    
    M = sla.LinearOperator(A.shape, _solve)
    return M


def block_factorization_preconditioner_ABF(A, maxiter=1, output=False):
    nelem = A.shape[0]/2
    App = A[0:nelem,0:nelem]
    Aps = A[0:nelem,nelem:nelem*2]
    Asp = A[nelem:nelem*2,0:nelem]
    Ass = A[nelem:nelem*2,nelem:nelem*2]

    # Alternate-Block Factorization(ABF) matrix decoupling
    Lambda = App.diagonal(0)*Ass.diagonal(0) - Aps.diagonal(0)*Asp.diagonal(0)
    Lambda_inv = sp.diags(1./Lambda, format='csr')
    Appstar = Lambda_inv*(sp.diags(Ass.diagonal(0), format='csr')*App - 
                          sp.diags(Aps.diagonal(0), format='csr')*Asp)
    Assstar = Lambda_inv*(sp.diags(App.diagonal(0), format='csr')*Ass - 
                          sp.diags(Asp.diagonal(0), format='csr')*Aps)

    # approximate Schur complement
    Ass_diag_inv = sp.diags(1./Assstar.diagonal(0), format="csr")
    Sbar = Appstar - (Aps*Ass_diag_inv)*Asp
    # initial guess
    u = np.zeros(A.shape[0])
    def _solve(b):

        niter = 0
        
        while True:
            # step 1
            r = b - A*u

            if niter >= maxiter:
                break
            rp = r[0:nelem]
            rs = r[nelem:nelem*2]
            
            # step 2
            #ml = pyamg.smoothed_aggregation_solver(Ass)
            #snew = ml.solve(rs, accel='gmres')
            res = []
            ml = pyamg.ruge_stuben_solver(Ass)
            snew = ml.solve(rs, accel='gmres', tol=1e-07, residuals=res)
            if output:
                print "    inner iterations: %i, residual: %g" % (len(res), 
                                                                  res[-1]) 
            #snew = sla.spsolve(Ass, rs)
            # step 3
            rnew = rp - Aps*snew
            # step 4
            #ml = pyamg.smoothed_aggregation_solver(Sbar)
            #pnew = ml.solve(rnew, accel='gmres')
            res = []
            ml2 = pyamg.ruge_stuben_solver(Sbar)
            pnew = ml2.solve(rnew, accel='gmres', tol=1e-07, residuals=res)
            if output:
                print "    inner iterations: %i, residual: %g" % (len(res), 
                                                                  res[-1]) 
            #pnew = sla.spsolve(Sbar, rnew)
            
            # update solution
            u[0:nelem] = u[0:nelem] + pnew
            u[nelem:nelem*2] = u[nelem:nelem*2] + snew
            niter += 1
        if output: 
            print "  precond. iter: %i"  % (niter)
        return u
    
    M = sla.LinearOperator(A.shape, _solve)
    return M


def my_lu(A,b):
    A = A.tolil()
    n = A.shape[0]
    for k in range(n-1):
        for i in range(k+1,n):
            A[i,k] = A[i,k]/A[k,k]
            for j in range(k+1,n):
                A[i,j] = A[i,j]-A[i,k]*A[k,j]
    
    def solve(b):
        n = b.shape[0]
        x = np.zeros(n)
        y = np.zeros(n)
        for i in range(n):
            alpha = 0
            for k in range(i):
                alpha = alpha + A[i,k]*y[k]
            y[i] = b[i] - alpha
        for i in range(n-1,-1,-1):
            alpha = 0
            for k in range(i+1,n):
                alpha = alpha + A[i,k]*x[k]
            x[i] = (y[i]- alpha)/A[i,i]
        return x
    
    return solve(b)

'''
A = pyamg.gallery.poisson((3,5), format='csr')
b = np.random.rand(A.shape[0])
x = np.zeros(A.shape[0])
np.set_printoptions(precision=3)

#M = jacobi_preconditioner(A)
#M = ilu_preconditioner(A)
M = my_ilu_preconditioner(A)
#print M*np.eye(M.shape[0])
counter = iter_counter('scipy')
x = sla.iterative.bicgstab(A, b, maxiter=100, tol=1e-07, M=M,
                               atol=1e-50, callback=counter)
print 'ilu iter: ', x[0]

M = jacobi_preconditioner(A)
#print M*np.eye(M.shape[0])
counter = iter_counter('scipy')
x = sla.iterative.bicgstab(A, b, maxiter=100, tol=1e-07, M=M,
                               atol=1e-50, callback=counter)
print 'jac iter: ', x[0]
'''
'''
data_path = '/home/heepark/models/simple_wipp/simple_2d/'
matrix_name = 'WFjacobian_ts9_tc0_ni0.bin'
vector_name = 'WFresidual_ts9_tc0_ni0.bin'
nx = 60
ny = 55
'''
'''
data_path = '/home/heepark/models/wipp/2d/s5_r2_v001/bcgs-ilu0-debug-1core/step290/'
matrix_name = 'WFjacobian_ts291_tc0_ni2.bin'
vector_name = 'WFresidual_ts291_tc0_ni2.bin'
nx = 68
ny = 33
'''


data_path = '/home/heepark/models/wipp/3d/s1_r3_v078/bcgs-ilu0-debug-1core/nonconvergence/'
matrix_name = 'WFjacobian_ts3_tc0_ni3.bin'
vector_name = 'WFresidual_ts3_tc0_ni3.bin'
#matrix_name = 'WFjacobian_ts4_tc0_ni3.bin'
#vector_name = 'WFresidual_ts4_tc0_ni3.bin'
#matrix_name = 'WFjacobian_ts7_tc0_ni3.bin'
#vector_name = 'WFresidual_ts7_tc0_ni3.bin'
#matrix_name = 'WFjacobian_ts11_tc0_ni3.bin'
#vector_name = 'WFresidual_ts11_tc0_ni3.bin'
#matrix_name = 'WFjacobian_ts20_tc0_ni1.bin'
#vector_name = 'WFresidual_ts20_tc0_ni1.bin'
#matrix_name = 'WFjacobian_ts20_tc1_ni3.bin'
#vector_name = 'WFresidual_ts20_tc1_ni3.bin'
#matrix_name = 'WFjacobian_ts22_tc0_ni3.bin'
#vector_name = 'WFresidual_ts22_tc0_ni3.bin'
#matrix_name = 'WFjacobian_ts26_tc0_ni1.bin'
#vector_name = 'WFresidual_ts26_tc0_ni1.bin'
#matrix_name = 'WFjacobian_ts35_tc0_ni3.bin'
#vector_name = 'WFresidual_ts35_tc0_ni3.bin'
nx = 68
ny = 33
nz = 61


A = load.petsc_binary_as_csr(data_path, matrix_name)
b = load.petsc_binary_vector(data_path, vector_name)
#newA = util.rearrange_matrix_2d_wipp_flow(A,nx,ny)
newA = util.rearrange_matrix_3d_wipp_flow(A,nx,ny,nz)
newb = util.rearrange_vector_wipp_flow(b)
#M = block_factorization_preconditioner(newA)

#counter = iter_counter('scipy')
#xx, status = sla.iterative.gmres(newA, newb, maxiter=10, tol=1e-07, M=M,
#                                 atol=1e-50, callback=counter)
#xx, status, info = solve.scipy_bicgstab(newA, newb, tol=1e-07, maxiter=10,
#                                        M_in=M)

'''
start = time.time()
counter = iter_counter('pyamg')
M = cpr1_quasi_IMPES_preconditioner(newA,output=True)
xx, status = pyamg.krylov.fgmres(newA,newb,tol=1e-7,M=M,callback=counter,
                                 restrt=10, maxiter=5)
print 'cpq iter: ', xx
print 'time: %.3f seconds' % (time.time()-start)
'''

start = time.time()
counter = iter_counter('pyamg')
M = cpr1_true_IMPES_preconditioner_row(newA, output=False)
xx, status = pyamg.krylov.fgmres(newA,newb,tol=1e-7,M=M,callback=counter,
                                 restrt=10, maxiter=5)
print 'cpt iter: ', xx
print 'time: %.3f seconds' % (time.time()-start)


'''
start = time.time()
counter = iter_counter('pyamg')
M = cpr1_true_IMPES_preconditioner(newA, output=False)
xx, status = pyamg.krylov.fgmres(newA,newb,tol=1e-7,M=M,callback=counter,
                                 restrt=10, maxiter=5)
print 'cpt iter: ', xx
print 'time: %.3f seconds' % (time.time()-start)

start = time.time()
counter = iter_counter('pyamg')
M = cpr1_ABF_preconditioner(newA, output=False)
xx, status = pyamg.krylov.fgmres(newA,newb,tol=1e-7,M=M,callback=counter, 
                                 restrt=10, maxiter=5)
print 'c1a iter: ', xx
print 'time: %.3f seconds' % (time.time()-start)

start = time.time()
counter = iter_counter('pyamg')
M = cpr2_ABF_preconditioner(newA, output=True)
xx, status = pyamg.krylov.fgmres(newA,newb,tol=1e-7,M=M,callback=counter, 
                                 restrt=10, maxiter=5)
print 'c2a iter: ', xx
print 'time: %.3f seconds' % (time.time()-start)



start = time.time()
counter = iter_counter('pyamg')
M = block_factorization_preconditioner(newA, output=True)
xx, status = pyamg.krylov.fgmres(newA,newb,tol=1e-7,M=M,callback=counter)
print 'bf iter : ', xx
print 'time: %.3f seconds' % (time.time()-start)


start = time.time()
#M = ilu_preconditioner(A)
M = ilu_preconditioner(newA)
counter = iter_counter('pyamg')
#xxx, status = pyamg.krylov.fgmres(A,b,tol=1e-7,M=M,callback=counter)
xxx, status = pyamg.krylov.bicgstab(newA,newb,tol=1e-7,M=M,callback=counter)
print 'ilu iter: ', xxx
print 'time: %.3f seconds' % (time.time()-start)

start = time.time()
#x = sla.spsolve(A, b)
x = sla.spsolve(newA, newb)
print 'direct  : ', x
print 'time: %.3f seconds' % (time.time()-start)

'''

start = time.time()
counter = iter_counter('pyamg')
M = block_factorization_preconditioner(newA, output=True)
xx, status = pyamg.krylov.fgmres(newA,newb,tol=1e-7,M=M,callback=counter)
print 'bf iter : ', xx
print 'time: %.3f seconds' % (time.time()-start)

