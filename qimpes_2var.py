#!/usr/bin/env python2
# -*- coding: utf-8 -*-
"""
Created on Thu Dec 26 15:35:14 2019

@author: heepark
"""

import pyamg
import utility as util
import plot
import importlib
import numpy as np
import scipy.sparse as sp
import scipy.sparse.linalg as sla
import numpy.linalg as nla
import time

nx=50
ny=50
nz=2

nvar=2
m = util.create_test_2d_matrix_rand(nx,ny,nvar)
#plot.plot_matrix(m)

nelem = int(m.shape[0]/2)

newm = util.rearrange_matrix_2d_wipp_flow(m,nx,ny)
#plot.plot_matrix(newm)

m = m.tocoo()

rows = m.shape[0]
cols = m.shape[1]
block_size = nvar

row_end = rows-1
row_start = 0

num_blocks = int(rows/block_size)
num_blocks_local = int(((row_end+1)-row_start)/block_size)

all_values = np.zeros([2,20])
col_index_keep = np.zeros([20],dtype=int)
insert_col_index = np.zeros([15],dtype=int)
insert_vals = np.zeros([15])
local_factors = np.zeros([block_size])

A_p = np.zeros([int(rows/2),int(cols/2)])

start_time = time.time()
for i in range(0,num_blocks_local):
    all_values = np.zeros([2,20])
    col_index_keep = np.zeros([20])

    firstrow = i*block_size + row_start
    numcols = m.getrow(firstrow).nnz
    col_index = np.flip(m.getrow(firstrow).nonzero()[1])
    values = np.flip(m.getrow(firstrow).data)

    for k in range(0,numcols):
        all_values[0, k] = values[k]
        col_index_keep[k] = int(col_index[k])

    firstrowdex = -1
    for loopdex in range(0,numcols,2):
        if (col_index[loopdex] == firstrow):
            firstrowdex = loopdex

    numcols_keep = np.copy(numcols)
    for j in range(1,block_size):
        numcols = m.getrow(firstrow+j).nnz
        values = np.flip(m.getrow(firstrow+j).data)
        for k in range(0,numcols):
            all_values[j,k] = values[k]

    # extract the diagonal block which is a jacobian of the element itself
    # and it consists of 
    # j_pp, j_ps
    # j_sp, j_ss
    # in a variable, diag_block, below.
    diag_block = all_values[0:block_size,firstrowdex:firstrowdex+block_size]

    e_ps = diag_block[0,1]
    e_ss = diag_block[1,1]
    e_ss_inv = 1./e_ss

    insert_rows = i + int(row_start/block_size)
    ncolblocks = int(numcols_keep/block_size)

    for j in range(0,ncolblocks):
        cur_coldex = int(j*block_size)
        insert_col_index[j] = int(col_index_keep[cur_coldex]/block_size)

        insert_vals[j] = all_values[0, cur_coldex] \
                         - e_ps*e_ss_inv*all_values[1, cur_coldex]
               
    for j in range(0,ncolblocks):
        A_p[insert_rows,insert_col_index[j]] = insert_vals[j]
        insert_col_index[j] = 0
        insert_vals[j] = 0.0
end_time = time.time()

A_p = sp.csr_matrix(A_p)
plot.plot_matrix(A_p)


App = newm[0:nelem, 0:nelem]
Aps = newm[0:nelem, nelem:nelem*2]
Asp = newm[nelem:nelem*2, 0:nelem]
Ass = newm[nelem:nelem*2, nelem:nelem*2]
u = np.zeros(newm.shape[0])

# Quasi IMPES matrix decoupling
Ass_diag_inv = sp.diags(1./Ass.diagonal(0), format="csr")
Aps_diag = sp.diags(Aps.diagonal(0), format="csr")
Appstar = App - Aps_diag*Ass_diag_inv*Asp

plot.plot_matrix(Appstar)
    
norm = sla.norm(Appstar - A_p)
if norm < 5.e-14:
    print("Passed. Norm: %1.5e" % norm)
else:
    print("Failed. Norm: %1.5e" % norm)
print("Compute Time: %1.5g [s]" % (end_time-start_time))
