import scipy.sparse as sp
import scipy.sparse.linalg as sla
import numpy.linalg as nla
import numpy as np
from scipy import rand, ravel, log10, kron, eye

import petsc4py
import sys
from petsc4py import PETSc
petsc4py.init(sys.argv)
import pyamg
import inspect


class iter_counter(object):
    def __init__(self, module):
        self.niter = 0
        self.res = []
        self.module = module
    def __call__(self, xk=None):
        self.niter += 1
        frame = inspect.currentframe().f_back
        if self.module == 'scipy':
            self.res.append(frame.f_locals['resid'])
            print('iter %3i\tres = %e' % (self.niter, self.res[self.niter-1]))
        elif self.module == 'pyamg':
            self.res.append(frame.f_locals['normr'])
            print('iter %3i\tres = %e' % (self.niter, self.res[self.niter-1]))


def print_iteration(niter,u,b,r,rel_norm):
    ind = abs(r).argmax()
    print (("\nOUTER_ITER %i") % niter )
    print (("  index: %i\tu: %g\tb: %g\tr: %g\trel_norm: %g") % \
    (ind, u[ind], b[ind], r[ind], rel_norm))
    return


def block_factorization(A, b, rtol=1.e-8, maxiter=100):
    nelem = A.shape[0]/2
    App = A[0:nelem,0:nelem]
    Aps = A[0:nelem,nelem:nelem*2]
    Asp = A[nelem:nelem*2,0:nelem]
    Ass = A[nelem:nelem*2,nelem:nelem*2]
    # norm of RHS
    r_norm_old = nla.norm(b)
    # approximate Schur complement
    Ass_diag_inv = sp.diags(1./Ass.diagonal(0), format="csr")
    Sbar = App - (Aps*Ass_diag_inv)*Asp
    # initial guess
    u = np.zeros(len(b))
    niter = 0
    
    while True:
        # step 1
        r = b - A*u
        r_norm = nla.norm(r)
        
        rel_norm = r_norm/r_norm_old
        print_iteration(niter,u,b,r,rel_norm)
        if rel_norm < rtol:
            break
        if niter > maxiter:
            break
        rp = r[0:nelem]
        rs = r[nelem:nelem*2]
        
        # step 2
        snew, msg_s = pyamg_smooth_aggregation(Ass,rs,output=False)
        # step 3
        rnew = rp - Aps*snew
        # step 4
        pnew, msg_p = pyamg_smooth_aggregation(Sbar,rnew,output=False)
        
        # update solution
        u[0:nelem] = u[0:nelem] + pnew
        u[nelem:nelem*2] = u[nelem:nelem*2] + snew
        niter += 1
        
    return u


def cpr1_true_IMPES(A, b, rtol=1.e-10, maxiter=100):
    nelem = A.shape[0]/2
    App = A[0:nelem, 0:nelem]
    Aps = A[0:nelem, nelem:nelem*2]
    Asp = A[nelem:nelem*2, 0:nelem]
    Ass = A[nelem:nelem*2, nelem:nelem*2]
    u = np.zeros(len(b))
    r_norm_old = nla.norm(b)

    # true-IMPES matrix conversion decoupling
    # .sum operations is column sum operation
    Ass_colsum_inv = sp.diags(np.array(1./Ass.sum(axis=0))[0], 
                              format='csr')
    Aps_colsum = sp.diags(np.array(Aps.sum(axis=0))[0], format='csr')
    Appstar = App - Aps_colsum*Ass_colsum_inv*Asp
    niter = 0
    
    # For step 2
    ILU = sla.spilu(A, permc_spec='NATURAL')
    
    while True:
        # step 1
        r = b - A*u
        r_norm = nla.norm(r)
        
        rel_norm = r_norm/r_norm_old
        print_iteration(niter,u,b,r,rel_norm)
        if rel_norm < rtol:
            break
        if niter > maxiter:
            break
        
        # step 2
        u_ilu = ILU.solve(r)
        uhalf = u + u_ilu
        
        # step 3
        rhalf = r - A*u_ilu

        # step 4
        rp = rhalf[0:nelem]
        rs = rhalf[nelem:nelem*2]
        rp = rp - Aps_colsum*Ass_colsum_inv*rs
        del_p, msg_p = pyamg_smooth_aggregation(Appstar,rp,output=False)
        
        # step 5
        u[0:nelem] = uhalf[0:nelem] + del_p
        u[nelem:nelem*2] = uhalf[nelem:nelem*2]
        
        niter += 1
    
    return u


def cpr1_quasi_IMPES(A, b, rtol=1.e-10, maxiter=100):
    nelem = A.shape[0]/2
    App = A[0:nelem, 0:nelem]
    Aps = A[0:nelem, nelem:nelem*2]
    Asp = A[nelem:nelem*2, 0:nelem]
    Ass = A[nelem:nelem*2, nelem:nelem*2]
    u = np.zeros(len(b))
    r_norm_old = nla.norm(b)

    # quasi-IMPES matrix decoupling
    Ass_diag_inv = sp.diags(1./Ass.diagonal(0), format="csr")
    Aps_diag = sp.diags(Aps.diagonal(0), format="csr")
    Appstar = App - Aps_diag*Ass_diag_inv*Asp
    niter = 0
    
    # For step 2
    ILU = sla.spilu(A, permc_spec='NATURAL')
    
    while True:
        # step 1
        r = b - A*u
        r_norm = nla.norm(r)
        
        rel_norm = r_norm/r_norm_old
        print_iteration(niter,u,b,r,rel_norm)
        if rel_norm < rtol:
            break
        if niter > maxiter:
            break
        
        # step 2
        u_ilu = ILU.solve(r)
        uhalf = u + u_ilu
        
        # step 3
        rhalf = r - A*u_ilu

        # step 4
        rp = rhalf[0:nelem]
        rs = rhalf[nelem:nelem*2]
        rp = rp - Aps_diag*Ass_diag_inv*rs
        del_p, msg_p = pyamg_smooth_aggregation(Appstar,rp,output=False)
        
        # step 5
        u[0:nelem] = uhalf[0:nelem] + del_p
        u[nelem:nelem*2] = uhalf[nelem:nelem*2]
        
        niter += 1
    
    return u


def cpr1_ABF(A, b, rtol=1.e-10, maxiter=100):
    nelem = A.shape[0]/2
    App = A[0:nelem, 0:nelem]
    Aps = A[0:nelem, nelem:nelem*2]
    Asp = A[nelem:nelem*2, 0:nelem]
    Ass = A[nelem:nelem*2, nelem:nelem*2]
    u = np.zeros(len(b))
    r_norm_old = nla.norm(b)

    # Alternate-Block Factorization(ABF) matrix decoupling
    Lambda = App.diagonal(0)*Ass.diagonal(0) - Aps.diagonal(0)*Asp.diagonal(0)
    Lambda_inv = sp.diags(1./Lambda, format='csr')
    Appstar = Lambda_inv*(sp.diags(Ass.diagonal(0), format='csr')*App - 
                          sp.diags(Aps.diagonal(0), format='csr')*Asp)  
    niter = 0
    
    # For step 2
    ILU = sla.spilu(A, permc_spec='NATURAL')
    
    while True:
        # step 1
        r = b - A*u
        r_norm = nla.norm(r)
        
        rel_norm = r_norm/r_norm_old
        print_iteration(niter,u,b,r,rel_norm)
        if rel_norm < rtol:
            break
        if niter > maxiter:
            break
        
        # step 2
        u_ilu = ILU.solve(r)
        uhalf = u + u_ilu
        
        # step 3
        rhalf = r - A*u_ilu

        # step 4
        rp = rhalf[0:nelem]
        rs = rhalf[nelem:nelem*2]
        rp = Lambda_inv*(Ass.diagonal(0)*rp-Aps.diagonal(0)*rs)
        del_p, msg_p = pyamg_smooth_aggregation(Appstar,rp,output=False)
        
        # step 5
        u[0:nelem] = uhalf[0:nelem] + del_p
        u[nelem:nelem*2] = uhalf[nelem:nelem*2]
        
        niter += 1
    
    return u


def cpr2_ABF(A, b, rtol=1.e-10, maxiter=100):
    nelem = A.shape[0]/2
    App = A[0:nelem, 0:nelem]
    Aps = A[0:nelem, nelem:nelem*2]
    Asp = A[nelem:nelem*2, 0:nelem]
    Ass = A[nelem:nelem*2, nelem:nelem*2]
    u = np.zeros(len(b))
    r_norm_old = nla.norm(b)

    # Alternate-Block Factorization(ABF) matrix decoupling
    Lambda = App.diagonal(0)*Ass.diagonal(0) - Aps.diagonal(0)*Asp.diagonal(0)
    Lambda_inv = sp.diags(1./Lambda, format='csr')
    Appstar = Lambda_inv*(sp.diags(Ass.diagonal(0), format='csr')*App - 
                          sp.diags(Aps.diagonal(0), format='csr')*Asp)
    Assstar = Lambda_inv*(sp.diags(App.diagonal(0), format='csr')*Ass - 
                          sp.diags(Asp.diagonal(0), format='csr')*Aps)
    niter = 0
    
    # For step 2
    ILU = sla.spilu(A, permc_spec='NATURAL')
    
    while True:
        # step 1
        r = b - A*u
        r_norm = nla.norm(r)
        
        rel_norm = r_norm/r_norm_old
        print_iteration(niter,u,b,r,rel_norm)
        if rel_norm < rtol:
            break
        if niter > maxiter:
            break
        
        # step 2
        u_ilu = ILU.solve(r)
        uhalf = u + u_ilu
        
        # step 3
        rhalf = r - A*u_ilu
        
        # step 4
        rp = rhalf[0:nelem]
        rs = rhalf[nelem:nelem*2]
          # decouple pressure residual
        rpstar = Lambda_inv*(Ass.diagonal(0)*rp-Aps.diagonal(0)*rs)
        del_p, msg_p = pyamg_smooth_aggregation(Appstar,rpstar,output=False)
        
        # step 5
          # decouple pressure residual
        rsstar = Lambda_inv*(App.diagonal(0)*rs-Asp.diagonal(0)*rp)
        del_s, msg_s = pyamg_smooth_aggregation(Assstar,rsstar,output=False)
               
        # step 6
        u[0:nelem] = uhalf[0:nelem] + del_p
        u[nelem:nelem*2] = uhalf[nelem:nelem*2] + del_s

        niter += 1
    
    return u


def scipy_direct(A, b):
    x = sla.spsolve(A, b)
    return x


def scipy_bicgstab(A, b, x0=None, maxiter=1000, tol=1e-08, M_in=None,
                   preconditioner='', output=True):
    if output:
        counter = iter_counter('scipy')
    else:
        counter = None

    if x0 == None:
        x0 = np.zeros(b.shape[0])
    
    if M_in == None:
        if preconditioner == 'jacobi':
            M = jacobi_preconditioner(A)
        elif preconditioner == 'ilu':
            M = ilu_preconditioner(A)
        elif preconditioner == 'amg':
            M = amg_preconditioner(A)
        else:
            M = None
    else:
        M = M_in
    
    x = sla.iterative.bicgstab(A, b, x0, maxiter=maxiter, tol=tol, M=M,
                               atol=1e-50, callback=counter)
    status = x[1]
    x = x[0]
    
    return x, status, counter


def pyamg_smooth_aggregation(A,b,tol=1e-7,output=True,option=1,omega=4/3.):
    if output:
        counter = iter_counter('pyamg')
    else:
        counter = None

    if option == 1:
        ml = pyamg.smoothed_aggregation_solver(A)
    elif option == 2:
        ml = pyamg.smoothed_aggregation_solver(A,
             presmoother=('jacobi', {'iterations': 5, 'omega':omega}),
             postsmoother=('jacobi', {'iterations': 5, 'omega':omega}))
    elif option == 3:
        ml = pyamg.smoothed_aggregation_solver(A,
             presmoother=('gauss_seidel', {'iterations': 3}),
             postsmoother=('gauss_seidel', {'iterations': 3}))
    elif option == 4:
        ml = pyamg.smoothed_aggregation_solver(A,
             presmoother=('schwarz', {'iterations': 2}),
             postsmoother=('schwarz', {'iterations': 2}))
    elif option == 5:
        ml = pyamg.smoothed_aggregation_solver(A,
             presmoother=('block_jacobi', {'iterations': 5, 'omega':omega, 'blocksize' : 3}),
             postsmoother=('block_jacobi', {'iterations': 5, 'omega':omega, 'blocksize' : 3}))
    elif option == 6:
        ml = pyamg.smoothed_aggregation_solver(A,
             presmoother=('block_gauss_seidel', {'iterations': 3, 'blocksize' : 3}),
             postsmoother=('block_gauss_seidel', {'iterations': 3, 'blocksize' : 3}))
    elif option == 7:
        ml = pyamg.smoothed_aggregation_solver(A,
             presmoother=('sor', {'iterations': 5, 'omega':omega}),
             postsmoother=('sor', {'iterations': 5, 'omega':omega}))
    elif option == 8:
        ml = pyamg.smoothed_aggregation_solver(A,
             presmoother=('gauss_seidel_ne', {'iterations': 3}),
             postsmoother=('gauss_seidel_ne', {'iterations': 3}))
    elif option == 9:
        ml = pyamg.smoothed_aggregation_solver(A,
             presmoother=('gauss_seidel_nr', {'iterations': 3}),
             postsmoother=('gauss_seidel_nr', {'iterations': 3}))
    elif option == 10:
        ml = pyamg.smoothed_aggregation_solver(A, max_levels=1)
    '''
    ml = pyamg.smoothed_aggregation_solver(A,
    presmoother=('gauss_seidel_nr', {'iterations': 5}))
    '''
    res = []
    x = ml.solve(b, tol=tol, maxiter=300, accel='gmres', callback=counter)

    return x, counter


def pyamg_block_smooth_aggregation(A,b,tol=1e-7,output=True,option=6,omega=4/3):
    if output:
        counter = iter_counter('pyamg')
    else:
        counter = None
    A = A.tobsr((3,3))
   
    if option == 1:
        ml = pyamg.smoothed_aggregation_solver(A)
    elif option == 2:
        ml = pyamg.smoothed_aggregation_solver(A,
             presmoother=('jacobi', {'iterations': 5, 'omega':omega}),
             postsmoother=('jacobi', {'iterations': 5, 'omega':omega}))
    elif option == 3:
        ml = pyamg.smoothed_aggregation_solver(A,
             presmoother=('gauss_seidel', {'iterations': 3}),
             postsmoother=('gauss_seidel', {'iterations': 3}))
    elif option == 4:
        ml = pyamg.smoothed_aggregation_solver(A,
             presmoother=('schwarz', {'iterations': 2}),
             postsmoother=('schwarz', {'iterations': 2}))
    elif option == 5:
        ml = pyamg.smoothed_aggregation_solver(A,
             presmoother=('block_jacobi', {'iterations': 5, 'omega':omega}),
             postsmoother=('block_jacobi', {'iterations': 5, 'omega':omega}))
    elif option == 6:
        ml = pyamg.smoothed_aggregation_solver(A,
             presmoother=('block_gauss_seidel', {'iterations': 3}),
             postsmoother=('block_gauss_seidel', {'iterations': 3}))
    elif option == 7:
        ml = pyamg.smoothed_aggregation_solver(A,
             presmoother=('sor', {'iterations': 5, 'omega':omega}),
             postsmoother=('sor', {'iterations': 5, 'omega':omega}))
    elif option == 8:
        ml = pyamg.smoothed_aggregation_solver(A,
             presmoother=('gauss_seidel_ne', {'iterations': 3}),
             postsmoother=('gauss_seidel_ne', {'iterations': 3}))
    elif option == 9:
        ml = pyamg.smoothed_aggregation_solver(A,
             presmoother=('gauss_seidel_nr', {'iterations': 3}),
             postsmoother=('gauss_seidel_nr', {'iterations': 3}))
        
    res = []
    x = ml.solve(b, tol=tol, maxiter=100, accel='bicgstab', callback=counter)

    return x, counter


def pyamg_optimized(A,b,output=True):
    if output:
        counter = iter_counter('pyamg')
    else:
        counter = None

    B = np.ones((A.shape[0],1), dtype=A.dtype); BH = B.copy()
    res = []
    ml = pyamg.smoothed_aggregation_solver(A, B=B, BH=BH,
        strength=('evolution', {'epsilon': 2.0, 'k': 2, 'proj_type': 'l2'}),
        smooth=('energy', {'weighting': 'local', 'krylov': 'gmres', 'degree': 1, 'maxiter': 2}),
        improve_candidates=None,
        aggregate="standard",
        presmoother=('gauss_seidel_nr', {'sweep': 'symmetric', 'iterations': 2}),
        postsmoother=('gauss_seidel_nr', {'sweep': 'symmetric', 'iterations': 2}),
        max_levels=15,
        max_coarse=300,
        coarse_solver="pinv")
    
    x = ml.solve(b, tol=1e-08, residuals=res, accel="bicgstab", maxiter=300, cycle="W", callback=counter)

    return x, counter


def pyamg_block_optimized(A,b,output=True):
    if output:
        counter = iter_counter('pyamg')
    else:
        counter = None

    B = kron(np.ones((A.shape[0]/A.blocksize[0],1), dtype=A.dtype), eye(A.blocksize[0])); BH = B.copy()
    
    ml = pyamg.smoothed_aggregation_solver(A, B=B, BH=BH,
        strength=('evolution', {'epsilon': 4.0, 'k': 2, 'proj_type': 'l2'}),
        smooth=('energy', {'weighting': 'local', 'krylov': 'gmres', 'degree': 3, 'maxiter': 4}),
        improve_candidates=None,
        aggregate="standard",
        presmoother=('gauss_seidel_nr', {'sweep': 'symmetric', 'iterations': 2}),
        postsmoother=('gauss_seidel_nr', {'sweep': 'symmetric', 'iterations': 2}),
        max_levels=15,
        max_coarse=300,
        coarse_solver="pinv")

    res = []
    x = ml.solve(b, tol=1e-08, residuals=res, accel="bicgstab", maxiter=300, cycle="W", callback=counter)
    
    return x, counter

def my_ilu_preconditioner(A):
    A=A.toarray()
    for k in range(A.shape[0]-1):
        for i in range(k+1,A.shape[0]):
            if abs(A[i,k]) > 1.0e-30:
                A[i,k] = A[i,k]/A[k,k]
            for j in range(k+1,A.shape[0]):
                if abs(A[i,j]) > 1.0e-30:
                    A[i,j] = A[i,j]-A[i,k]*A[k,j]
    return sp.csr_matrix(A)


def jacobi_preconditioner(A):
    M = sp.diags(1./A.diagonal()).tocsr()
    return M


def ilu_preconditioner(A, drop_tol=1e-5, fill_factor=10):
    ILU = sla.spilu(A, drop_tol=drop_tol, fill_factor=fill_factor, permc_spec='NATURAL')
    M = sla.LinearOperator(A.shape, ILU.solve)
    return M


def amg_preconditioner(A):
    ml = pyamg.smoothed_aggregation_solver(A, symmetry='nonsymmetric',
                                           strength='evolution',
                                           smooth='energy')
    M = ml.aspreconditioner()
    return M


def mumps_direct(A, b):
    petsc_A = PETSc.Mat().createAIJ(size=A.shape,
                                    csr=(A.indptr, A.indices, A.data))
    petsc_b = PETSc.Vec().createWithArray(b)
    ksp = PETSc.KSP().create()
    ksp.setOperators(petsc_A)
    ksp.setType('preonly')
    pc=ksp.getPC()
    pc.setType('lu')
    pc.setFactorSolverType('mumps')
    
    print ('Solving with: ', ksp.getType())
    print ('Preconditioning: ', pc.getType(), ' ', pc.getFactorSolverType())
    petsc_x = PETSc.Vec().createWithArray(np.zeros(np.size(b)))
    ksp.solve(petsc_b, petsc_x)
    x = petsc_x.array
    return x
    

def petsc_bicgstab_asm(A, b, ncore=1):
    petsc_A = PETSc.Mat().createAIJ(size=A.shape,
                                    csr=(A.indptr, A.indices, A.data))
    petsc_b = PETSc.Vec().createWithArray(b)
    ksp = PETSc.KSP().create()
    ksp.setOperators(petsc_A)
    ksp.setType('bcgs')
    ksp.setUp()
    pc=ksp.getPC()
    pc.setType('asm')
    pc.setUp()
    sub_ksp = pc.getASMSubKSP()
    for i in range(ncore):
        sub_ksp[i].setType('preonly')
        sub_pc = sub_ksp[i].getPC()
        sub_pc.setType('ilu')
        sub_pc.setUp()
        
    print ('Solving with: ', ksp.getType())
    print ('Preconditioning: ', pc.getType(), ' ', pc.getFactorSolverType())
    petsc_x = PETSc.Vec().createWithArray(np.zeros(np.size(b)))
    ksp.solve(petsc_b, petsc_x)
    x = petsc_x.array
    return ksp, pc, x

