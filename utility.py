import scipy.sparse as sp
import scipy.sparse.linalg as sla
import numpy.linalg as nla
import numpy as np

def abs_min_max(A):
    A = A.tocoo()
    Aposcol = A.col[A.sign().data == 1]
    pospos = A.col[A.sign().data == 1].copy()  # there is some bug with Aposcol
    Anegcol = A.col[A.sign().data == -1].copy()
    Aposrow = A.row[A.sign().data == 1].copy()
    Anegrow = A.row[A.sign().data == -1].copy()
    Aposdata = A.data[A.sign().data == 1].copy()
    Anegdata = A.data[A.sign().data == -1].copy()
    Anegdata = -1. * Anegdata
    if len(Anegdata) == 0:
        vabsmin = min(Aposdata.min(), Anegdata.min())
    else:
        vabsmin = Aposdata.min()
    if len(Anegdata) == 0:
        vabsmax = max(Aposdata.max(), Anegdata.max())
    else:
        vabsmax = Aposdata.max()

    return vabsmin, vabsmax

def check_zero_diag(A):
    diag = A.diagonal()
    diag = abs(diag)
    if (diag > 1.e-30).all():
        print('We good.')
    else:
        print('There are ZEROS in the diagonal (<1e-30)')

def rearrange_matrix_3d_wipp_flow(A,nx,ny,nz):
    # 2 unknowns per elements
    nelem = A.shape[0]/2
    if nelem != nx*ny*nz:
        print("number of elements in the matrix does not match nx*ny.")
        return

    # PP SS diagonal of the element
    diag = A.diagonal(0)
    newdiag = np.append(diag[0::2],diag[1::2])
    # PS diagonal of the element
    diag_ps = A.diagonal(1)
    newdiag_ps = diag_ps[0::2]
    # SP diagonal of the element
    diag_sp = A.diagonal(-1)
    newdiag_sp = diag_sp[0::2]

    # PP SS diagonal to the right element
    diagr = A.diagonal(2)
    newdiagr = np.append(diagr[0::2],np.zeros(1))
    newdiagr = np.append(newdiagr,diagr[1::2])
    # PS diagonal to the right element
    diag_psr = A.diagonal(2+1)
    newdiag_psr = diag_psr[0::2]
    # SP diagonal to the right element
    diag_spr = A.diagonal(2-1)
    newdiag_spr = np.append(np.zeros(1),diag_spr[1::2])
    newdiag_spr = np.append(newdiag_spr,np.zeros(1))

    # PP SS diagonal to the left element
    diagl = A.diagonal(-2)
    newdiagl = np.append(diagl[0::2],np.zeros(1))
    newdiagl = np.append(newdiagl,diagl[1::2])
    # PS diagonal to the left element
    diag_psl = A.diagonal(-2+1)
    newdiag_psl = np.append(np.zeros(1),diag_psl[1::2])
    newdiag_psl = np.append(newdiag_psl,np.zeros(1))
    # SP diagonal to the left element
    diag_spl = A.diagonal(-2-1)
    newdiag_spl = diag_spl[0::2]

    # PP SS diagonal to the north element
    diagnor = A.diagonal(nx*2)
    newdiagnor = np.append(diagnor[0::2],np.zeros(nx))
    newdiagnor = np.append(newdiagnor,diagnor[1::2])
    # PS diagonal to the north element
    diagnor_ps = A.diagonal(nx*2+1)
    newdiagnor_ps = diagnor_ps[0::2]
    # SP diagonal to the north element
    diagnor_sp = A.diagonal(nx*2-1)
    newdiagnor_sp = np.append(np.zeros(nx),diagnor_sp[1::2])
    newdiagnor_sp = np.append(newdiagnor_sp,np.zeros(nx))

    # PP SS diagonal to the south element
    diagsou = A.diagonal(-nx*2)
    newdiagsou = np.append(diagsou[0::2],np.zeros(nx))
    newdiagsou = np.append(newdiagsou,diagsou[1::2])
    # PS diagonal to the south element
    diagsou_ps = A.diagonal(-nx*2+1)
    newdiagsou_ps = np.append(np.zeros(nx),diagsou_ps[1::2])
    newdiagsou_ps = np.append(newdiagsou_ps,np.zeros(nx))
    # SP diagonal to the south element
    diagsou_sp = A.diagonal(-nx*2-1)
    newdiagsou_sp = diagsou_sp[0::2]

    # PP SS diagonal to the top element
    newdiagtop_pp = A.diagonal(nx*ny*2)[0::2]
    newdiagtop_ss = A.diagonal(nx*ny*2)[1::2]
    # PP SS diagonal to the bottom element
    newdiagbot_pp = A.diagonal(-nx*ny*2)[0::2]
    newdiagbot_ss = A.diagonal(-nx*ny*2)[1::2]
    # PS diagonal to the top element
    newdiagtop_ps = A.diagonal(nx*ny*2+1)[0::2]
    # PS diagonal to the bottom element
    newdiagbot_ps = A.diagonal(-nx*ny*2+1)[1::2]
    # SP diagonal to the top element
    newdiagtop_sp = A.diagonal(nx*ny*2-1)[1::2]
    # SP diagonal to the bottom element
    newdiagbot_sp = A.diagonal(-nx*ny*2-1)[0::2]
    if -nelem+nx*ny == -nx*ny:
        newdiagtop = np.append(newdiagtop_pp,newdiagbot_ps)
        newdiagtop = np.append(newdiagtop,newdiagtop_ss)
        newdiagbot = np.append(newdiagbot_pp,newdiagtop_sp)
        newdiagbot = np.append(newdiagbot,newdiagbot_ss)


        newA = sp.diags([newdiagbot_sp,
                         newdiagsou_sp, newdiag_spl, newdiag_sp, newdiag_spr, newdiagnor_sp,
                         newdiagbot,
                         newdiagsou, newdiagl, newdiag, newdiagr, newdiagnor,
                         newdiagtop,
                         newdiagsou_ps, newdiag_psl, newdiag_ps, newdiag_psr, newdiagnor_ps,
                         newdiagtop_ps
                         ],
                        [-nelem-nx*ny,
                         -nelem-nx, -nelem-1, -nelem, -nelem+1, -nelem+nx,
                         -nelem+nx*ny,
                         -nx, -1, 0, +1, +nx,
                         +nelem-nx*ny,
                         +nelem-nx, +nelem-1, +nelem, +nelem+1, +nelem+nx,
                         +nelem+nx*ny],
                         format="csr")
    else:
        newdiagtop = np.append(newdiagtop_pp,np.zeros(nx*ny))
        newdiagtop = np.append(newdiagtop,newdiagtop_ss)
        newdiagbot = np.append(newdiagbot_pp,np.zeros(nx*ny))
        newdiagbot = np.append(newdiagbot,newdiagbot_ss)
        newdiagbot_ps = np.append(np.zeros(nx*ny),newdiagbot_ps)
        newdiagbot_ps = np.append(newdiagbot_ps,np.zeros(nx*ny))
        newdiagtop_sp = np.append(np.zeros(nx*ny),newdiagtop_sp)
        newdiagtop_sp = np.append(newdiagtop_sp,np.zeros(nx*ny))


        newA = sp.diags([newdiagbot_sp,
                         newdiagsou_sp, newdiag_spl, newdiag_sp, newdiag_spr, newdiagnor_sp,
                         newdiagtop_sp,
                         newdiagbot,
                         newdiagsou, newdiagl, newdiag, newdiagr, newdiagnor,
                         newdiagtop,
                         newdiagbot_ps,
                         newdiagsou_ps, newdiag_psl, newdiag_ps, newdiag_psr, newdiagnor_ps,
                         newdiagtop_ps
                         ],
                        [-nelem-nx*ny,
                         -nelem-nx, -nelem-1, -nelem, -nelem+1, -nelem+nx,
                         -nelem+nx*ny,
                         -nx*ny,
                         -nx, -1, 0, +1, +nx,
                         +nx*ny,
                         +nelem-nx*ny,
                         +nelem-nx, +nelem-1, +nelem, +nelem+1, +nelem+nx,
                         +nelem+nx*ny],
                         format="csr")

    return newA

def rearrange_matrix_2d_wipp_flow(A,nx,ny):
    # 2 unknowns per elements
    nelem = int(A.shape[0]/2)
    if nelem != nx*ny:
        print("number of elements in the matrix does not match nx*ny.")
        return

    # PP SS diagonal of the element
    diag = A.diagonal(0)
    newdiag = np.append(diag[0::2],diag[1::2])
    # PS diagonal of the element
    diag_ps = A.diagonal(1)
    newdiag_ps = diag_ps[0::2]
    # SP diagonal of the element
    diag_sp = A.diagonal(-1)
    newdiag_sp = diag_sp[0::2]

    # PP SS diagonal to the right element
    diagr = A.diagonal(2)
    newdiagr = np.append(diagr[0::2],np.zeros(1))
    newdiagr = np.append(newdiagr,diagr[1::2])
    # PS diagonal to the right element
    diag_psr = A.diagonal(2+1)
    newdiag_psr = diag_psr[0::2]
    # SP diagonal to the right element
    diag_spr = A.diagonal(2-1)
    newdiag_spr = np.append(np.zeros(1),diag_spr[1::2])
    newdiag_spr = np.append(newdiag_spr,np.zeros(1))

    # PP SS diagonal to the left element
    diagl = A.diagonal(-2)
    newdiagl = np.append(diagl[0::2],np.zeros(1))
    newdiagl = np.append(newdiagl,diagl[1::2])
    # PS diagonal to the left element
    diag_psl = A.diagonal(-2+1)
    newdiag_psl = np.append(np.zeros(1),diag_psl[1::2])
    newdiag_psl = np.append(newdiag_psl,np.zeros(1))
    # SP diagonal to the left element
    diag_spl = A.diagonal(-2-1)
    newdiag_spl = diag_spl[0::2]

    # PP SS diagonal to the top element
    diagtop = A.diagonal(nx*2)
    newdiagtop = np.append(diagtop[0::2],np.zeros(nx))
    newdiagtop = np.append(newdiagtop,diagtop[1::2])
    # PS diagonal to the top element
    diagtop_ps = A.diagonal(nx*2+1)
    newdiagtop_ps = diagtop_ps[0::2]
    # SP diagonal to the top element
    diagtop_sp = A.diagonal(nx*2-1)
    newdiagtop_sp = np.append(np.zeros(nx),diagtop_sp[1::2])
    newdiagtop_sp = np.append(newdiagtop_sp,np.zeros(nx))

    # PP SS diagonal to the bottom element
    diagbot = A.diagonal(-nx*2)
    newdiagbot = np.append(diagbot[0::2],np.zeros(nx))
    newdiagbot = np.append(newdiagbot,diagbot[1::2])
    # PS diagonal to the bottom element
    diagbot_ps = A.diagonal(-nx*2+1)
    newdiagbot_ps = np.append(np.zeros(nx),diagbot_ps[1::2])
    newdiagbot_ps = np.append(newdiagbot_ps,np.zeros(nx))
    # SP diagonal to the bottom element
    diagbot_sp = A.diagonal(-nx*2-1)
    newdiagbot_sp = diagbot_sp[0::2]

    #NEW MATRIX ASSEMBLY

    newA = sp.diags([newdiagbot_sp, newdiag_spl, newdiag_sp, newdiag_spr,
                     newdiagtop_sp,
                     newdiagbot, newdiagl, newdiag, newdiagr, newdiagtop,
                     newdiagbot_ps, newdiag_psl, newdiag_ps, newdiag_psr,
                     newdiagtop_ps],
                    [-nelem-nx, -nelem-1, -nelem, -nelem+1, -nelem+nx,
                     -nx, -1, 0, 1, nx,
                     +nelem-nx, +nelem-1, +nelem, +nelem+1, +nelem+nx],
                     format="csr")

    return newA

def rearrange_matrix_1d_wipp_flow(A):
    # 2 unknowns per elements
    nelem = int(A.shape[0]/2)

    # main diagonal
    diag = A.diagonal(0)
    newdiag = np.append(diag[0::2],diag[1::2])
    # diagonal to the right element
    diagr = A.diagonal(2)
    newdiagr = np.append(diagr[0::2],np.zeros(1))
    newdiagr = np.append(newdiagr,diagr[1::2])
    # diagonal to the left element
    diagl = A.diagonal(-2)
    newdiagl = np.append(diagl[0::2],np.zeros(1))
    newdiagl = np.append(newdiagl,diagl[1::2])

    # PS main diagonal
    diag_ps = A.diagonal(1)
    newdiag_ps = diag_ps[0::2]
    # PS diagonal to the right element
    diag_psr = A.diagonal(1+2)
    newdiag_psr = diag_psr[0::2]
    # PS diagonal to the left element
    diag_psl = A.diagonal(1-2)
    newdiag_psl = np.append(np.zeros(1),diag_psl[1::2])
    newdiag_psl = np.append(newdiag_psl,np.zeros(1))
    # SP main diagonal
    diag_sp = A.diagonal(-1)
    newdiag_sp = diag_sp[0::2]
    # SP diagonal to the right element
    diag_spr = A.diagonal(-1+2)
    newdiag_spr = np.append(np.zeros(1),diag_spr[1::2])
    newdiag_spr = np.append(newdiag_spr,np.zeros(1))
    # SP diagonal to the left element
    diag_spl = A.diagonal(-1-2)
    newdiag_spl = diag_spl[0::2]

    #NEW MATRIX ASSEMBLY

    newA = sp.diags([newdiag_spl, newdiag_sp, newdiag_spr,
                     newdiagl,newdiag,newdiagr,
                     newdiag_psl,newdiag_ps,newdiag_psr],
                    [-nelem-1,-nelem,-nelem+1,
                     -1,0,1,
                     nelem-1,nelem,nelem+1],
                     format="csr")

    return newA

def rearrange_matrix_1d_general(A):
    # 3 unknowns per elements
    nelem = A.shape[0]/3

    # main diagonal
    diag = A.diagonal(0)
    newdiag = np.append(diag[0::3],[diag[1::3],diag[2::3]])
    # diagonal to the right element
    diagr = A.diagonal(3)
    newdiagr = np.append(diagr[0::3],[np.zeros(1)])
    newdiagr = np.append(newdiagr,[diagr[1::3]])
    newdiagr = np.append(newdiagr,[np.zeros(1)])
    newdiagr = np.append(newdiagr,[diagr[2::3]])
    # diagonal to the left element
    diagl = A.diagonal(-3)
    newdiagl = np.append(diagl[0::3],[np.zeros(1)])
    newdiagl = np.append(newdiagl,[diagl[1::3]])
    newdiagl = np.append(newdiagl,[np.zeros(1)])
    newdiagl = np.append(newdiagl,[diagl[2::3]])

    # PS, ST main diagonal
    diag_ps_st = A.diagonal(1)
    newdiag_ps_st = np.append(diag_ps_st[0::3],[diag_ps_st[1::3]])
    # PS, ST diagonal to the right element
    diag_ps_str = A.diagonal(1+3)
    newdiag_ps_str = np.append(diag_ps_str[0::3],[np.zeros(1)])
    newdiag_ps_str = np.append(newdiag_ps_str,[diag_ps_str[1::3]])
    # PS, ST diagonal to the left element
    diag_ps_stl = A.diagonal(1-3)
    newdiag_ps_stl = np.append(np.zeros(1),[diag_ps_stl[1::3]])
    newdiag_ps_stl = np.append(newdiag_ps_stl,[np.zeros(1)])
    newdiag_ps_stl = np.append(newdiag_ps_stl,[diag_ps_stl[2::3]])
    newdiag_ps_stl = np.append(newdiag_ps_stl,[np.zeros(1)])

    # SP, TS main diagonal
    diag_sp_ts = A.diagonal(-1)
    newdiag_sp_ts = np.append(diag_sp_ts[0::3],[diag_sp_ts[1::3]])
    # SP, TS diagonal to the right element
    diag_sp_tsr = A.diagonal(-1+3)
    newdiag_sp_tsr = np.append(np.zeros(1),[diag_sp_tsr[1::3]])
    newdiag_sp_tsr = np.append(newdiag_sp_tsr,[np.zeros(1)])
    newdiag_sp_tsr = np.append(newdiag_sp_tsr,[diag_sp_tsr[2::3]])
    newdiag_sp_tsr = np.append(newdiag_sp_tsr,[np.zeros(1)])
    # SP, TS diagonal to the left element
    diag_sp_tsl = A.diagonal(-1-3)
    newdiag_sp_tsl = np.append(diag_sp_tsl[0::3],[np.zeros(1)])
    newdiag_sp_tsl = np.append(newdiag_sp_tsl,[diag_sp_tsl[1::3]])

    # PT main diagonal
    diag_pt = A.diagonal(2)
    newdiag_pt = diag_pt[0::3]
    # PT diagonal to the right element
    diag_ptr = A.diagonal(2+3)
    newdiag_ptr = diag_ptr[0::3]
    # PT diagonal to the left element
    diag_ptl = A.diagonal(2-3)
    newdiag_ptl = np.append(np.zeros(1),[diag_ptl[2::3]])
    newdiag_ptl = np.append(newdiag_ptl,[np.zeros(1)])

    # TP main diagonal
    diag_tp = A.diagonal(-2)
    newdiag_tp = diag_tp[0::3]
    # TP diagonal to the right element
    diag_tpr = A.diagonal(-2+3)
    newdiag_tpr = np.append(np.zeros(1),[diag_tpr[2::3]])
    newdiag_tpr = np.append(newdiag_tpr,[np.zeros(1)])
    # TP diagonal to the left element
    diag_tpl = A.diagonal(-2-3)
    newdiag_tpl = diag_tpl[0::3]

    #NEW MATRIX ASSEMBLY

    newA = sp.diags([newdiag_tpl,newdiag_tp,newdiag_tpr,
                     newdiag_sp_tsl,newdiag_sp_ts,newdiag_sp_tsr,
                     newdiagl,newdiag,newdiagr,
                     newdiag_ps_stl,newdiag_ps_st,newdiag_ps_str,
                     newdiag_ptl,newdiag_pt,newdiag_ptr],
                    [-nelem-nelem-1,-nelem-nelem,-nelem-nelem+1,
                     -nelem-1,-nelem,-nelem+1,
                     -1,0,1,
                     nelem-1,nelem,nelem+1,
                     nelem+nelem-1,nelem+nelem,nelem+nelem+1],
                    format="csr")

    return newA

def rearrange_vector_general(x):
    newx = np.append(x[0::3],[x[1::3],x[2::3]])
    return newx

def rearrange_vector_wipp_flow(x):
    newx = np.append(x[0::2],x[1::2])
    return newx

def remove_negative_diagonals(A,b):
    # A in csr format
    diag = A.diagonal()
    ind = np.arange(0,diag.shape[0])
    indices = ind[diag < 0.0]

    for index in indices:
        A[index] = A[index]*-1.0
        b[index] = b[index]*-1.0

    return A,b


def create_test_2d_matrix(nx,ny,nvar):
    nelem = nx*ny

    A = np.zeros([nelem*nvar,nelem*nvar],dtype='int')

    # create element variable.
    elem = np.zeros([nvar,nvar],dtype='int')
    for i in range(nvar):
        for j in range(nvar):
            elem[i,j] = i*nvar + j + 1

    zero = np.zeros([nvar,nvar],dtype='int')

    # element itself
    for i in range(0,nelem):
        ind = i*nvar
        A[ind:ind+nvar,ind:ind+nvar] = elem

    # element to the right
    for i in range(0,nelem-1):
        ind = i*nvar
        if (i+1)%nx == 0:
            A[ind:ind+nvar,ind+nvar:ind+nvar*2] = zero
        else:
            A[ind:ind+nvar,ind+nvar:ind+nvar*2] = elem

    # element to the left
    for i in range(0,nelem-1):
        ind = i*nvar
        if (i+1)%nx == 0:
            A[ind+nvar:ind+nvar*2,ind:ind+nvar] = zero
        else:
            A[ind+nvar:ind+nvar*2,ind:ind+nvar] = elem

    # element to the top
    for i in range(0,nelem-nx):
        ind = i*nvar
        nxv = nvar*nx
        A[ind:ind+nvar,ind+nxv:ind+nvar+nxv] = elem

    # element to the top
    for i in range(0,nelem-nx):
        ind = i*nvar
        nxv = nvar*nx
        A[ind+nxv:ind+nvar+nxv,ind:ind+nvar] = elem

    A = sp.csr_matrix(A)

    return A


def create_test_2d_matrix_distinct(nx,ny,nvar):
    nelem = nx*ny

    A = np.zeros([nelem*nvar,nelem*nvar],dtype='int')

    # create element variable.
    elem = np.zeros([nvar,nvar],dtype='int')
    for i in range(nvar):
        for j in range(nvar):
            elem[i,j] = i*nvar + j + 1

    zero = np.zeros([nvar,nvar],dtype='int')

    # element itself
    for i in range(0,nelem):
        ind = i*nvar
        A[ind:ind+nvar,ind:ind+nvar] = elem

    # element to the right
    for i in range(0,nelem-1):
        ind = i*nvar
        if (i+1)%nx == 0:
            A[ind:ind+nvar,ind+nvar:ind+nvar*2] = zero
        else:
            A[ind:ind+nvar,ind+nvar:ind+nvar*2] = elem + 10

    # element to the left
    for i in range(0,nelem-1):
        ind = i*nvar
        if (i+1)%nx == 0:
            A[ind+nvar:ind+nvar*2,ind:ind+nvar] = zero
        else:
            A[ind+nvar:ind+nvar*2,ind:ind+nvar] = elem - 10

    # element to the top
    for i in range(0,nelem-nx):
        ind = i*nvar
        nxv = nvar*nx
        A[ind:ind+nvar,ind+nxv:ind+nvar+nxv] = elem + 20

    # element to the top
    for i in range(0,nelem-nx):
        ind = i*nvar
        nxv = nvar*nx
        A[ind+nxv:ind+nvar+nxv,ind:ind+nvar] = elem - 20

    A = sp.csr_matrix(A)

    return A


def create_test_2d_matrix_rand(nx,ny,nvar):
    nelem = nx*ny

    A = np.zeros([nelem*nvar,nelem*nvar],dtype='float')

    # create element variable.
    elem = np.zeros([nvar,nvar],dtype='float')
    for i in range(nvar):
        for j in range(nvar):
            elem[i,j] = i*nvar + j + 1

    zero = np.zeros([nvar,nvar],dtype='float')

    # element itself
    for i in range(0,nelem):
        ind = i*nvar
        A[ind:ind+nvar,ind:ind+nvar] = elem + np.random.rand(nvar,nvar)*3

    # element to the right
    for i in range(0,nelem-1):
        ind = i*nvar
        if (i+1)%nx == 0:
            A[ind:ind+nvar,ind+nvar:ind+nvar*2] = zero
        else:
            A[ind:ind+nvar,ind+nvar:ind+nvar*2] = elem + 10 + np.random.rand(nvar,nvar)*3

    # element to the left
    for i in range(0,nelem-1):
        ind = i*nvar
        if (i+1)%nx == 0:
            A[ind+nvar:ind+nvar*2,ind:ind+nvar] = zero
        else:
            A[ind+nvar:ind+nvar*2,ind:ind+nvar] = elem - 10 - np.random.rand(nvar,nvar)*3

    # element to the top
    for i in range(0,nelem-nx):
        ind = i*nvar
        nxv = nvar*nx
        A[ind:ind+nvar,ind+nxv:ind+nvar+nxv] = elem + 20 + np.random.rand(nvar,nvar)*3

    # element to the bottom
    for i in range(0,nelem-nx):
        ind = i*nvar
        nxv = nvar*nx
        A[ind+nxv:ind+nvar+nxv,ind:ind+nvar] = elem - 20 - np.random.rand(nvar,nvar)*3

    A = sp.csr_matrix(A)

    return A


def create_test_3d_matrix(nx,ny,nz,nvar):
    nelem = nx*ny*nz

    A = np.zeros([nelem*nvar,nelem*nvar],dtype='int')

    # create element variable.
    elem = np.zeros([nvar,nvar],dtype='int')
    for i in range(nvar):
        for j in range(nvar):
            elem[i,j] = i*nvar + j + 1#0

    zero = np.zeros([nvar,nvar],dtype='int')

    # element itself
    for i in range(0,nelem):
        ind = i*nvar
        A[ind:ind+nvar,ind:ind+nvar] = elem

    # element to the right
    for i in range(0,nelem-1):
        ind = i*nvar
        if (i+1)%nx == 0:
            A[ind:ind+nvar,ind+nvar:ind+nvar*2] = zero
        else:
            A[ind:ind+nvar,ind+nvar:ind+nvar*2] = elem #+ 2.5

    # element to the left
    for i in range(0,nelem-1):
        ind = i*nvar
        if (i+1)%nx == 0:
            A[ind+nvar:ind+nvar*2,ind:ind+nvar] = zero
        else:
            A[ind+nvar:ind+nvar*2,ind:ind+nvar] = elem #- 2.5

    # element to the north
    nxv = nvar*nx
    fill = range(nx*ny-nx)
    counter = 0
    for i in range(0,nelem-nx):
        ind = i*nvar
        if counter in fill:
            A[ind:ind+nvar,ind+nxv:ind+nvar+nxv] = elem #+ 5
        else:
            A[ind:ind+nvar,ind+nxv:ind+nvar+nxv] = zero

        if counter == nx*ny-1:
            counter = 0
        else:
            counter += 1

    # element to the south
    nxv = nvar*nx
    fill = range(nx*ny-nx)
    counter = 0
    for i in range(0,nelem-nx):
        ind = i*nvar
        if counter in fill:
            A[ind+nxv:ind+nvar+nxv,ind:ind+nvar] = elem #- 5
        else:
            A[ind+nxv:ind+nvar+nxv,ind:ind+nvar] = zero

        if counter == nx*ny-1:
            counter = 0
        else:
            counter += 1

    # element to the top
    nxv = nvar*nx*ny
    for i in range(0,nelem-nx*ny):
        ind = i*nvar
        A[ind:ind+nvar,ind+nxv:ind+nvar+nxv] = elem #+ 7.5

    # element to the bottom
    nxv = nvar*nx*ny
    for i in range(0,nelem-nx*ny):
        ind = i*nvar
        A[ind+nxv:ind+nvar+nxv,ind:ind+nvar] = elem #- 7.5

    A = sp.csr_matrix(A)

    return A


def create_test_3d_matrix_rand(nx,ny,nz,nvar):
    nelem = nx*ny*nz

    A = np.zeros([nelem*nvar,nelem*nvar],dtype='int')

    # create element variable.
    elem = np.zeros([nvar,nvar],dtype='int')
    for i in range(nvar):
        for j in range(nvar):
            elem[i,j] = i*nvar + j + 1#0

    zero = np.zeros([nvar,nvar],dtype='int')

    # element itself
    for i in range(0,nelem):
        ind = i*nvar
        A[ind:ind+nvar,ind:ind+nvar] = elem + np.random.rand(nvar,nvar)*3

    # element to the right
    for i in range(0,nelem-1):
        ind = i*nvar
        if (i+1)%nx == 0:
            A[ind:ind+nvar,ind+nvar:ind+nvar*2] = zero
        else:
            A[ind:ind+nvar,ind+nvar:ind+nvar*2] = elem + 10.0 + np.random.rand(nvar,nvar)*3

    # element to the left
    for i in range(0,nelem-1):
        ind = i*nvar
        if (i+1)%nx == 0:
            A[ind+nvar:ind+nvar*2,ind:ind+nvar] = zero
        else:
            A[ind+nvar:ind+nvar*2,ind:ind+nvar] = elem - 10.0 - np.random.rand(nvar,nvar)*3
            
    # element to the north
    nxv = nvar*nx
    fill = range(nx*ny-nx)
    counter = 0
    for i in range(0,nelem-nx):
        ind = i*nvar
        if counter in fill:
            A[ind:ind+nvar,ind+nxv:ind+nvar+nxv] = elem + 20.0 + np.random.rand(nvar,nvar)*3
        else:
            A[ind:ind+nvar,ind+nxv:ind+nvar+nxv] = zero

        if counter == nx*ny-1:
            counter = 0
        else:
            counter += 1

    # element to the south
    nxv = nvar*nx
    fill = range(nx*ny-nx)
    counter = 0
    for i in range(0,nelem-nx):
        ind = i*nvar
        if counter in fill:
            A[ind+nxv:ind+nvar+nxv,ind:ind+nvar] = elem - 20.0 - np.random.rand(nvar,nvar)*3
        else:
            A[ind+nxv:ind+nvar+nxv,ind:ind+nvar] = zero

        if counter == nx*ny-1:
            counter = 0
        else:
            counter += 1

    # element to the top
    nxv = nvar*nx*ny
    for i in range(0,nelem-nx*ny):
        ind = i*nvar
        A[ind:ind+nvar,ind+nxv:ind+nvar+nxv] = elem + 30.0 + np.random.rand(nvar,nvar)*3

    # element to the bottom
    nxv = nvar*nx*ny
    for i in range(0,nelem-nx*ny):
        ind = i*nvar
        A[ind+nxv:ind+nvar+nxv,ind:ind+nvar] = elem - 30.0 - np.random.rand(nvar,nvar)*3

    A = sp.csr_matrix(A)

    return A
























